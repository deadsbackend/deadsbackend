import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { AppModule } from '../src/app.module';
import * as cookieParser from 'cookie-parser';
import * as request from 'supertest';
import { readdir } from 'fs/promises';
import { resetUserCredentials, seedUserCredentials } from './seed/userCredentials';
import { resetUserDetails, seedUserDetail } from './seed/userDetail';
import { resetItems, seedItems } from './seed/items';
import { AppRoutes } from '../src/lib/appRoutes';
import { userCredentialStub } from '../src/user/tests/stubs/UserCredential.stub';
import * as path from 'path';
import { PrismaClient } from '@prisma/client';
import { resetItemImages } from './seed/itemImages';

export const itemImageControllerTests = () => {
  const prisma = new PrismaClient();

  let app: INestApplication;
  let accessToken: string;
  let itemId: string;
  let imageFiles: string[];

  beforeAll(async () => {
    // TODO: Refactor test application initialization into separate file
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    app.use(cookieParser());
    app.useGlobalPipes(new ValidationPipe());
    await app.init();

    await seedUserCredentials(prisma);
    await seedUserDetail(prisma);
    await seedItems(prisma);

    const res = await request(app.getHttpServer()).post(AppRoutes.auth.login).send({
      email: userCredentialStub()[0].email,
      password: userCredentialStub()[0].password,
    });
    accessToken = res.body.userTokens.accessToken;

    imageFiles = await readdir(path.join(__dirname, 'seed/images'));
  });

  describe('Authenticated user', () => {
    test('it should get itemId from API', async () => {
      const res = await request(app.getHttpServer())
        .get(AppRoutes.items.root)
        .auth(accessToken, { type: 'bearer' })
        .send();
      expect(res.body[0].itemId).toBeDefined();
      itemId = res.body[0].itemId;
    });
    test('it should upload 2 images to this itemId', async () => {
      const res = await request(app.getHttpServer())
        .post(`${AppRoutes.itemImages.upload}/${itemId}`)
        .auth(accessToken, { type: 'bearer' })
        .attach('images', path.join(__dirname, `seed/images/${imageFiles[0]}`))
        .attach('images', path.join(__dirname, `seed/images/${imageFiles[1]}`))
        .expect(201);
    });
    test('it should get 2 filenames for this itemId from API', async () => {
      const res = await request(app.getHttpServer())
        .get(`${AppRoutes.itemImages.root}/${itemId}`)
        .auth(accessToken, { type: 'bearer' })
        .send()
        .expect(200);
      expect(res.body.images.length).toEqual(2);
      imageFiles = res.body.images;
    });
  });

  afterAll(async () => {
    await resetItemImages(prisma);
    await resetItems(prisma);
    await resetUserDetails(prisma);
    await resetUserCredentials(prisma);
  });
};
