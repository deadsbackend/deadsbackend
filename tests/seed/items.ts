import { Item, PrismaClient } from '@prisma/client';
import { v4 as uuidv4 } from 'uuid';
import { itemsStub } from '../../src/item/tests/stubs/Items.stub';

export const seedItems = async (prisma: PrismaClient): Promise<Item[]> => {
  return Promise.all(
    itemsStub().map(async (item) => {
      const itemId = uuidv4();
      return prisma.item.create({
        data: {
          itemId,
          title: item.title,
          description: item.description,
          partNumber: item.partNumber,
          price: item.price,
        },
      });
    }),
  );
};

export const resetItems = async (prisma: PrismaClient) => {
  await prisma.item.deleteMany();
};
