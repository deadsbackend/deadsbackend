import { PrismaClient, UserDetail } from '@prisma/client';
import { v4 as uuidv4 } from 'uuid';
import { userDetailStub } from '../../src/user/tests/stubs/UserDetail.stub';
import { userCredentialStub } from '../../src/user/tests/stubs/UserCredential.stub';

export const seedUserDetail = async (prisma: PrismaClient): Promise<UserDetail | null> => {
  return prisma.userCredential
    .update({
      where: {
        email: userCredentialStub()[0].email,
      },
      data: {
        detail: {
          create: {
            detailId: uuidv4(),
            firstName: userDetailStub().firstName,
            updatedAt: new Date(Date.now()),
          },
        },
      },
    })
    .detail();
};

export const resetUserDetails = async (prisma: PrismaClient) => {
  await prisma.userDetail.deleteMany();
};
