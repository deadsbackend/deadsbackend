import { PrismaClient, UserCredential } from '@prisma/client';
import { v4 as uuidv4 } from 'uuid';
import { UserRole } from '../../src/user/types';
import { PasswordService } from '../../src/auth/password.service';
import { userCredentialStub } from '../../src/user/tests/stubs/UserCredential.stub';

export const seedUserCredentials = async (prisma: PrismaClient): Promise<UserCredential[]> => {
  const passwordService = new PasswordService();
  return Promise.all(
    userCredentialStub().map(async (user) => {
      const userId = uuidv4();
      const password = await passwordService.hashPassword(user.password);
      return prisma.userCredential.create({
        data: {
          userId,
          email: user.email,
          password,
          role: UserRole.USER,
        },
      });
    }),
  );
};

export const resetUserCredentials = async (prisma: PrismaClient) => {
  await prisma.userCredential.deleteMany();
};
