import { PrismaClient } from '@prisma/client';

export const resetItemImages = async (prisma: PrismaClient) => {
  await prisma.itemImage.deleteMany();
};
