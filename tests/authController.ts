import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { AppModule } from '../src/app.module';
import * as request from 'supertest';
import { resetUserCredentials, seedUserCredentials } from './seed/userCredentials';
import { AppRoutes } from '../src/lib/appRoutes';
import { userCredentialStub } from '../src/user/tests/stubs/UserCredential.stub';
import * as cookieParser from 'cookie-parser';
import { resetUserDetails, seedUserDetail } from './seed/userDetail';
import { Cookies, extractCookies } from './cookies/extractCookies';
import {
  invalidEmailUserLoginRequestStub,
  invalidPasswordUserLoginRequestStub,
} from '../src/auth/tests/stubs/UserLoginRequest.stub';
import { PrismaClient } from '@prisma/client';

/* eslint-disable @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-argument, @typescript-eslint/no-unsafe-assignment */

export const authControllerTests = () => {
  const prisma = new PrismaClient();
  let app: INestApplication;
  let cookies: Cookies;
  let accessToken: string;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    app.use(cookieParser());
    app.useGlobalPipes(new ValidationPipe());
    await app.init();

    await seedUserCredentials(prisma);
    await seedUserDetail(prisma);
  });

  describe('Authenticated user', () => {
    test('it should login user with valid credentials, return accessToken and set refreshToken cookie', (done) => {
      void request(app.getHttpServer())
        .post(AppRoutes.auth.login)
        .send({
          email: userCredentialStub()[0].email,
          password: userCredentialStub()[0].password,
        })
        .expect(201)
        .end((err, res) => {
          expect(res.body.userTokens.refreshToken).toBeDefined();
          cookies = extractCookies(res.headers);
          expect(cookies.refreshToken).toBeDefined();
          done();
        });
    });
    test('it should return new access and refresh tokens if refresh token in cookie is valid', (done) => {
      void request(app.getHttpServer())
        .get(AppRoutes.auth.refreshTokens)
        .set('Cookie', `refreshToken=${cookies.refreshToken.cookie}`)
        .send()
        .expect(200)
        .end((err, res) => {
          const newCookies = extractCookies(res.headers);
          expect(newCookies.refreshToken.cookie).not.toEqual(cookies.refreshToken.cookie);
          expect(res.body.userTokens.refreshToken).toBeDefined();
          cookies = extractCookies(res.headers);
          accessToken = res.body.userTokens.accessToken;
          done();
        });
    });
    test('it should logout user and remove refreshToken from cookies', (done) => {
      void request(app.getHttpServer())
        .post(AppRoutes.auth.logout)
        .auth(accessToken, { type: 'bearer' })
        .set('Cookie', `refreshToken=${cookies.refreshToken.cookie}`)
        .send()
        .expect(200)
        .end((err, res) => {
          const newCookies = extractCookies(res.headers);
          expect(newCookies.refreshToken.cookie).toEqual('');
          done();
        });
    });
  });

  describe('Non-authenticated user', () => {
    describe('Test validation rules', () => {
      test('it should response Bad Request when provided email is invalid format', () => {
        void request(app.getHttpServer())
          .post(AppRoutes.auth.login)
          .send({
            email: invalidEmailUserLoginRequestStub().email,
            password: invalidEmailUserLoginRequestStub().password,
          })
          .expect(400);
      });
      test('it should response Bad Request when provided password is invalid format', () => {
        void request(app.getHttpServer())
          .post(AppRoutes.auth.login)
          .send({
            email: invalidPasswordUserLoginRequestStub().email,
            password: invalidPasswordUserLoginRequestStub().password,
          })
          .expect(400);
      });
    });
    describe('Test unauthorized login parameters', () => {
      test('it should response Unauthorized when there is no user with provided email', () => {
        void request(app.getHttpServer())
          .post(AppRoutes.auth.login)
          .send({
            email: 'incorrect@email.com',
            password: userCredentialStub()[0].password,
          })
          .expect(401);
      });
      test('it should response Unauthorized when provided password incorrect', () => {
        void request(app.getHttpServer())
          .post(AppRoutes.auth.login)
          .send({
            email: userCredentialStub()[0].email,
            password: 'invalid-password',
          })
          .expect(401);
      });
    });
    describe('Test public and private routes', () => {
      test('it should allow unathourized user to get public routes', () => {
        void request(app.getHttpServer()).get(AppRoutes.items.root).send().expect(200);
      });
      test('it should deny unauthorized user to get private routes', () => {
        void request(app.getHttpServer()).get(AppRoutes.users.root).send().expect(401);
      });
    });
  });

  afterAll(async () => {
    await app.close();

    await resetUserDetails(prisma);
    await resetUserCredentials(prisma);
  });
};

/* eslint-disable @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-argument, @typescript-eslint/no-unsafe-assignment */
