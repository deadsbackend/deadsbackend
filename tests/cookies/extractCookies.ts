interface CookieFlags {
  flagString: string;

  [key: string]: string;
}

export interface Cookies {
  [key: string]: {
    cookie: string;
    flags: CookieFlags;
  };
}

interface Headers {
  'set-cookie': string[];

  [key: string]: string[];
}

export const extractFlags = (flags: string[]): CookieFlags => {
  return flags.reduce(
    (readyFlags, flag) => {
      const [flagName, rawValue] = flag.split('=');
      const value = rawValue ? rawValue.replace(';', '') : true;
      return { ...readyFlags, [flagName]: value };
    },
    { flagString: flags.join('; ') },
  );
};

export const extractCookies = (headers: Headers) => {
  const cookies = headers['set-cookie'];

  return cookies.reduce((readyCookies: Cookies, cookieString) => {
    const [rawCookie, ...flags] = cookieString.split('; ');
    const [cookieName, value] = rawCookie.split('=');
    return { ...readyCookies, [cookieName]: { cookie: value, flags: extractFlags(flags) } };
  }, {});
};
