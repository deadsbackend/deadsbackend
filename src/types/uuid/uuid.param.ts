import { IsNotEmpty, IsUUID } from 'class-validator';

export class UuidParam {
  @IsNotEmpty()
  @IsUUID()
  id: string;
}
