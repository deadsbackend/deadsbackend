import { INestApplication, Injectable, OnModuleInit } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { ApiConfigService } from './config/apiConfig.service';

@Injectable()
export class PrismaService extends PrismaClient implements OnModuleInit {
  constructor(private apiConfigService: ApiConfigService) {
    let prismaLogConfig = {};
    if (apiConfigService.nodeEnvProd || apiConfigService.nodeEnvTest) {
      prismaLogConfig = { log: ['warn', 'error'] };
    } else {
      prismaLogConfig = { log: ['query', 'info', 'warn', 'error'] };
    }
    super(prismaLogConfig);
  }

  async onModuleInit() {
    await this.$connect();
  }

  enableShutdownHooks(app: INestApplication) {
    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    this.$on('beforeExit', async () => {
      await app.close();
    });
  }
}
