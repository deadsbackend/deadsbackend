export enum UserCredentialErrorMessages {
  INVALID_UUID = 'userId should be valid UUID',
  INVALID_EMAIL = 'email should be valid e-mail address',
  INVALID_UNIQUE_PARAMETER = 'invalid userId or email',
  INVALID_PASSWORD_LENGTH = 'password should be between 8 and 64 characters long',
  USER_CREDENTIAL_NOT_FOUND = 'user credential not found',
  ERROR_CREATING_USER_CREDENTIAL = 'error creating user credential',
  DUPLICATE_USER_CREDENTIAL = 'duplicate user credential',
  ERROR_CREATING_USER_DETAIL = 'error creating user detail',
  USER_DETAIL_NOT_FOUND = 'user detail not found',
  ERROR_UPDATING_USER_DETAIL = 'error updating user detail',
}
