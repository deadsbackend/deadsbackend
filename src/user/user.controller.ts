import { Body, Controller, Get, HttpException, HttpStatus, Param, Patch, Post } from '@nestjs/common';
import { UserService } from './user.service';
import { UserCredentialsCreateDto } from './dto/UserCredentials.create.dto';
import { UserCredentialsResponseDto } from './dto/UserCredentials.response.dto';
import { UserUniqueAttrsRequestDto } from './dto/UserUniqueAttrs.request.dto';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { UserDetailCreateDto } from './dto/UserDetail.create.dto';
import { UserDetailResponseDto } from './dto/UserDetail.response.dto';
import { UserDetailUpdateDto } from './dto/UserDetail.update.dto';
import { Public } from '../auth/jwtAuth.guard';
import { UserCredentialErrorMessages } from './types/userCredentialErrorMessages';

@ApiTags('Users')
@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Get()
  @ApiOkResponse({
    description: 'Returns all users credentials',
    type: [UserCredentialsResponseDto],
  })
  @ApiBearerAuth()
  async getAllUsersCredentials(): Promise<UserCredentialsResponseDto[]> {
    return this.userService.getAllUsersCredentials();
  }

  @Get('/unique')
  @ApiOkResponse({
    description: 'Returns specific user credentials',
    type: UserCredentialsResponseDto,
  })
  @ApiBadRequestResponse({
    description: 'Response when invalid userId or email is sent in request',
  })
  @ApiNotFoundResponse({
    description: 'User credential not found',
  })
  @ApiBearerAuth()
  async getUserCredentialByUniqueAttr(
    @Body() uniqueAttrs: UserUniqueAttrsRequestDto,
  ): Promise<UserCredentialsResponseDto> {
    if (uniqueAttrs.userId || uniqueAttrs.email) {
      return this.userService.getUniqueUserCredential(uniqueAttrs);
    } else {
      throw new HttpException(
        { message: UserCredentialErrorMessages.INVALID_UNIQUE_PARAMETER },
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  @Public()
  @Post('/create')
  @ApiCreatedResponse({
    description: 'Newly created UserCredential object',
    type: UserCredentialsResponseDto,
  })
  @ApiBadRequestResponse({
    description: 'Response when invalid email or password specified in request',
  })
  @ApiConflictResponse({
    description: 'Response when trying to create user credential which already exists',
  })
  async createUserCredential(@Body() userCredentials: UserCredentialsCreateDto): Promise<UserCredentialsResponseDto> {
    return this.userService.createUserCredentials(userCredentials);
  }

  @Get('/:userId/detail')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Returns user detail',
    type: UserDetailResponseDto,
  })
  @ApiNotFoundResponse({
    description: 'Response when no user detail found',
  })
  async getUserDetail(@Param('userId') userId: string): Promise<UserDetailResponseDto> {
    return this.userService.getUserDetail(userId);
  }

  @Post('/:userId/detail')
  @ApiCreatedResponse({
    description: 'Returns newly created user detail',
    type: UserDetailResponseDto,
  })
  @ApiConflictResponse({
    description: 'Response when trying to create user detail that already exists',
  })
  @ApiInternalServerErrorResponse({
    description: 'Unrecognized server error',
  })
  @ApiBearerAuth()
  async createUserDetail(
    @Param('userId') userId: string,
    @Body() userDetail: UserDetailCreateDto,
  ): Promise<UserDetailResponseDto> {
    return this.userService.createOrUpdateUserDetail(userId, userDetail);
  }

  @Patch('/:userId/detail')
  @ApiOkResponse({
    description: 'Returns updated user detail',
    type: UserDetailResponseDto,
  })
  @ApiNotFoundResponse({
    description: 'Response when trying to update not-existent user detail',
  })
  @ApiBearerAuth()
  async updateUserDetail(
    @Param('userId') userId: string,
    @Body() userDetail: UserDetailUpdateDto,
  ): Promise<UserDetailResponseDto> {
    return this.userService.createOrUpdateUserDetail(userId, userDetail);
  }
}
