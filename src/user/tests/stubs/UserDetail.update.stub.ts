import { UserDetailUpdateDto } from '../../dto/UserDetail.update.dto';

export const userDetailUpdateStub = (): UserDetailUpdateDto => {
  return {
    firstName: 'updated-first-name',
  };
};
