import { UserUniqueAttrsRequestDto } from '../../dto/UserUniqueAttrs.request.dto';
import { userCredentialStub } from './UserCredential.stub';

export const userUniqueAttrsRequestStub = (): UserUniqueAttrsRequestDto => {
  return {
    email: userCredentialStub()[0].email,
    userId: userCredentialStub()[0].userId,
  };
};

export const emptyUserUniqueAttrsRequestStub = (): UserUniqueAttrsRequestDto => {
  return {
    email: '',
    userId: '',
  };
};

export const invalidUserUniqueAttrsRequestStub = (): UserUniqueAttrsRequestDto => {
  return {
    email: 'invalid@email.com',
  };
};
