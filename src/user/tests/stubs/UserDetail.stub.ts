import { UserDetail } from '@prisma/client';

export const userDetailStub = (): UserDetail => {
  return {
    id: 1,
    detailId: '4e74f07b-b1ba-4bd0-bf58-0bce3f2fff5b',
    userCredentialId: 1,
    firstName: 'Ivan',
    lastName: null,
    birthDate: new Date('1990-12-04'),
    createdAt: new Date('2022-04-22'),
    updatedAt: new Date('2022-04-22'),
  };
};
