import { UserCredential } from '@prisma/client';
import { UserRole } from '../../types';
import { invalidRefreshTokenPayload, userTokensStub } from '../../../auth/tests/stubs/UserTokens.stub';

export const userCredentialStub = (): UserCredential[] => {
  return [
    {
      id: 1,
      userId: '5604281c-a0ac-4219-a5a5-5101a5728afd',
      email: 'root@ibm.com',
      password: 'very-secret-password',
      role: UserRole.USER,
      refreshToken: userTokensStub().refreshToken,
      createdAt: new Date('2022-04-21'),
      updatedAt: new Date('2022-04-21'),
    },
    {
      id: 2,
      userId: '0d988fc0-5e4b-4e06-a757-6795c131e4e9',
      email: 'root@sun.com',
      password: 'another-secret-password',
      role: UserRole.USER,
      refreshToken: userTokensStub().refreshToken,
      createdAt: new Date('2022-04-22'),
      updatedAt: new Date('2022-04-22'),
    },
  ];
};

export const invalidUserCredentialStub = (): UserCredential => {
  return {
    id: 999999,
    userId: 'invalid-id',
    email: 'root@xxx.com',
    password: 'invalid-password',
    role: UserRole.USER,
    refreshToken: invalidRefreshTokenPayload(),
    createdAt: new Date('2022-04-21'),
    updatedAt: new Date('2022-04-21'),
  };
};
