import { Test } from '@nestjs/testing';
import { PrismaModule } from '../../prisma.module';
import { UserService } from '../user.service';
import { PasswordService } from '../../auth/password.service';
import { UserCredentialsResponseDto } from '../dto/UserCredentials.response.dto';
import { invalidUserCredentialStub, userCredentialStub } from './stubs/UserCredential.stub';
import { UserCredentialErrorMessages } from '../types/userCredentialErrorMessages';
import { UserDetailResponseDto } from '../dto/UserDetail.response.dto';
import { userDetailUpdateStub } from './stubs/UserDetail.update.stub';
import { userDetailStub } from './stubs/UserDetail.stub';
import { DisabledLogger } from '../../lib/logger/DisabledLogger';

jest.mock('../../prisma.service');
jest.mock('../../auth/password.service');

describe('UserService', () => {
  let userService: UserService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [PrismaModule],
      providers: [UserService, PasswordService],
    }).compile();
    moduleRef.useLogger(new DisabledLogger());

    userService = moduleRef.get<UserService>(UserService);
  });

  test('it should be defined', () => {
    expect(userService).toBeDefined();
  });

  describe('createUserCredential', () => {
    test('it should return created user credential', async () => {
      const user = await userService.createUserCredentials({
        email: 'test@email.com',
        password: 'test-password',
      });
      expect(user).toBeDefined();
      expect(user).toBeInstanceOf(UserCredentialsResponseDto);
      expect(user.userId).toBeDefined();
    });
    test('it should throw HttpException if provided email exists', async () => {
      await expect(() =>
        userService.createUserCredentials({
          email: userCredentialStub()[0].email,
          password: 'test-password',
        }),
      ).rejects.toThrow(UserCredentialErrorMessages.DUPLICATE_USER_CREDENTIAL);
    });
  });

  describe('getAllUsersCredentials', () => {
    test('it should return all users credentials', async () => {
      const credentials = await userService.getAllUsersCredentials();
      expect(credentials).toBeDefined();
      expect(credentials).toBeInstanceOf(Array);
      expect(credentials.length).toEqual(userCredentialStub().length);
    });
  });

  describe('getUniqueUserCredential', () => {
    test('it should return user credential for valid userId', async () => {
      const user = await userService.getUniqueUserCredential({ userId: userCredentialStub()[0].userId });
      expect(user).toBeDefined();
      expect(user).toBeInstanceOf(UserCredentialsResponseDto);
      expect(user.userId).toEqual(userCredentialStub()[0].userId);
    });
    test('it should return user credential for valid email', async () => {
      const user = await userService.getUniqueUserCredential({ email: userCredentialStub()[0].email });
      expect(user).toBeDefined();
      expect(user).toBeInstanceOf(UserCredentialsResponseDto);
      expect(user.email).toEqual(userCredentialStub()[0].email);
    });
    test('it should throw HttpException for non-existent userId', async () => {
      await expect(() =>
        userService.getUniqueUserCredential({ userId: invalidUserCredentialStub().userId }),
      ).rejects.toThrow(UserCredentialErrorMessages.USER_CREDENTIAL_NOT_FOUND);
    });
    test('it should throw HttpException for non-existent email', async () => {
      await expect(() =>
        userService.getUniqueUserCredential({ userId: invalidUserCredentialStub().email }),
      ).rejects.toThrow(UserCredentialErrorMessages.USER_CREDENTIAL_NOT_FOUND);
    });
  });

  describe('getUserDetail', () => {
    test('it should return user detail for valid userId', async () => {
      const userDetail = await userService.getUserDetail(userCredentialStub()[0].userId);
      expect(userDetail).toBeDefined();
      expect(userDetail).toBeInstanceOf(UserDetailResponseDto);
    });
    test('it should throw HttpException for invalid userId', async () => {
      await expect(() => userService.getUserDetail(invalidUserCredentialStub().userId)).rejects.toThrow(
        UserCredentialErrorMessages.USER_DETAIL_NOT_FOUND,
      );
    });
  });

  describe('createOrUpdateUserDetail', () => {
    test('it should return updated user detail for valid userId', async () => {
      const userDetail = await userService.createOrUpdateUserDetail(
        userCredentialStub()[0].userId,
        userDetailUpdateStub(),
      );
      expect(userDetail).toBeDefined();
      expect(userDetail).toBeInstanceOf(UserDetailResponseDto);
      expect(userDetail.firstName).toEqual(userDetailStub().firstName);
    });
    test('it should throw HttpException for invalid userId', async () => {
      await expect(() =>
        userService.createOrUpdateUserDetail(invalidUserCredentialStub().userId, userDetailUpdateStub()),
      ).rejects.toThrow(UserCredentialErrorMessages.USER_CREDENTIAL_NOT_FOUND);
    });
  });
});
