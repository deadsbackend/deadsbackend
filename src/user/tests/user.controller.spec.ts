import { Test } from '@nestjs/testing';
import { UserController } from '../user.controller';
import { invalidUserCredentialStub, userCredentialStub } from './stubs/UserCredential.stub';
import { UserCredentialsResponseDto } from '../dto/UserCredentials.response.dto';
import { emptyUserUniqueAttrsRequestStub, userUniqueAttrsRequestStub } from './stubs/UserUniqueAttrsRequest.stub';
import { UserUniqueAttrsRequestDto } from '../dto/UserUniqueAttrs.request.dto';
import { UserCredentialErrorMessages } from '../types/userCredentialErrorMessages';
import { userDetailStub } from './stubs/UserDetail.stub';
import { UserDetailResponseDto } from '../dto/UserDetail.response.dto';
import { DisabledLogger } from '../../lib/logger/DisabledLogger';
import { UserService } from '../user.service';

jest.mock('../user.service');

describe('UserController', () => {
  let userController: UserController;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [UserController],
      providers: [UserService],
    }).compile();
    moduleRef.useLogger(new DisabledLogger());

    userController = moduleRef.get<UserController>(UserController);
  });

  test('it should be defined', () => {
    expect(userController).toBeDefined();
  });

  describe('getAllUsersCredentials', () => {
    test('it should return all users credentials', async () => {
      const users = await userController.getAllUsersCredentials();
      expect(users).toBeInstanceOf(Array);
      expect(users.length).toEqual(userCredentialStub().length);
    });
  });

  describe('getUserCredentialByUniqueAttr', () => {
    test('it should return user credential for valid userId', async () => {
      const uniqueAttrs: UserUniqueAttrsRequestDto = { ...userUniqueAttrsRequestStub(), email: '' };
      const userCredential = await userController.getUserCredentialByUniqueAttr(uniqueAttrs);
      expect(userCredential).toBeInstanceOf(UserCredentialsResponseDto);
      expect(userCredential.userId).toEqual(userCredentialStub()[0].userId);
    });

    test('it should return user credential for valid email', async () => {
      const uniqueAttrs: UserUniqueAttrsRequestDto = { ...userUniqueAttrsRequestStub(), userId: '' };
      const userCredential = await userController.getUserCredentialByUniqueAttr(uniqueAttrs);
      expect(userCredential).toBeInstanceOf(UserCredentialsResponseDto);
      expect(userCredential.email).toEqual(userCredentialStub()[0].email);
    });

    test('it should throw HttpException if no email or userId specified', async () => {
      await expect(() =>
        userController.getUserCredentialByUniqueAttr(emptyUserUniqueAttrsRequestStub()),
      ).rejects.toThrow(UserCredentialErrorMessages.INVALID_UNIQUE_PARAMETER);
    });
  });

  describe('createUserCredential', () => {
    test('it should return created user credential', async () => {
      const user = await userController.createUserCredential({
        email: userCredentialStub()[0].email,
        password: userCredentialStub()[0].password,
      });
      expect(user).toBeDefined();
      expect(user).toBeInstanceOf(UserCredentialsResponseDto);
      expect(user.email).toEqual(userCredentialStub()[0].email);
    });
  });

  describe('getUserDetail', () => {
    test('it should return user detail for provided userId', async () => {
      const userDetail = await userController.getUserDetail(userCredentialStub()[0].userId);
      expect(userDetail).toBeDefined();
      expect(userDetail).toBeInstanceOf(UserDetailResponseDto);
      expect(userDetail.detailId).toEqual(userDetailStub().detailId);
    });
  });

  describe('createUserDetail', () => {
    test('it should return created UserDetail', async () => {
      const userDetail = await userController.createUserDetail(userCredentialStub()[0].userId, {
        firstName: userDetailStub().firstName as string,
      });
      expect(userDetail).toBeDefined();
      expect(userDetail).toBeInstanceOf(UserDetailResponseDto);
      expect(userDetail.detailId).toEqual(userDetailStub().detailId);
    });
    test('it should throw HttpException for invalid userId', async () => {
      await expect(() =>
        userController.createUserDetail(invalidUserCredentialStub().userId, {
          firstName: userDetailStub().firstName as string,
        }),
      ).rejects.toThrow(UserCredentialErrorMessages.USER_CREDENTIAL_NOT_FOUND);
    });
  });

  describe('updateUserDetail', () => {
    test('it should return updated UserDetail', async () => {
      const userDetail = await userController.updateUserDetail(userCredentialStub()[0].userId, {
        birthDate: userDetailStub().birthDate as Date,
      });
      expect(userDetail).toBeDefined();
      expect(userDetail).toBeInstanceOf(UserDetailResponseDto);
      expect(userDetail.detailId).toEqual(userDetailStub().detailId);
    });
    test('it should throw HttpException for invalid userId', async () => {
      await expect(() =>
        userController.updateUserDetail(invalidUserCredentialStub().userId, {
          firstName: userDetailStub().firstName as string,
        }),
      ).rejects.toThrow(UserCredentialErrorMessages.USER_CREDENTIAL_NOT_FOUND);
    });
  });
});
