import { UserCredential } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';

export class UserCredentialsResponseDto {
  constructor(userId: string, email: string, updatedAt: Date) {
    this.userId = userId;
    this.email = email;
    this.updatedAt = updatedAt;
  }

  @ApiProperty({
    description: 'Unique user credential ID (UUIDv4)',
  })
  userId: string;

  @ApiProperty({
    description: 'E-mail address',
  })
  email: string;

  @ApiProperty({
    description: 'Last update time',
  })
  updatedAt: Date;

  static serialize(user: UserCredential): UserCredentialsResponseDto {
    return new UserCredentialsResponseDto(user.userId, user.email, user.updatedAt);
  }
}
