import { IsEmail, IsOptional, IsUUID } from 'class-validator';
import { UserCredentialErrorMessages } from '../types/userCredentialErrorMessages';
import { ApiProperty } from '@nestjs/swagger';

export class UserUniqueAttrsRequestDto {
  @ApiProperty({
    description: 'An UUID for user credential',
    example: '83043efa-d3bc-4f4c-9c98-4b3d090db610',
    required: false,
  })
  @IsOptional()
  @IsUUID('4', { message: UserCredentialErrorMessages.INVALID_UUID })
  userId?: string;

  @ApiProperty({
    description: 'An e-mail address for user credential',
    example: 'root@ibm.com',
    required: false,
  })
  @IsOptional()
  @IsEmail({}, { message: UserCredentialErrorMessages.INVALID_EMAIL })
  email?: string;
}
