import { UserDetail } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';

export class UserDetailResponseDto {
  constructor(
    detailId: string,
    birthDate: Date | null = null,
    firstName: string | null = null,
    lastName: string | null = null,
    updatedAt: Date = new Date(Date.now()),
  ) {
    this.detailId = detailId;
    this.birthDate = birthDate;
    this.firstName = firstName;
    this.lastName = lastName;
    this.updatedAt = updatedAt;
  }

  @ApiProperty({
    description: 'Unique user detail id (UUIDv4)',
  })
  detailId: string;

  @ApiProperty({
    description: "User's first name",
  })
  firstName?: string | null;

  @ApiProperty({
    description: "User's last name",
  })
  lastName?: string | null;

  @ApiProperty({
    description: "User's birth date",
  })
  birthDate?: Date | null;

  @ApiProperty({
    description: 'Last update time',
  })
  updatedAt: Date;

  static serialize(userDetail: UserDetail): UserDetailResponseDto {
    return new UserDetailResponseDto(
      userDetail.detailId,
      userDetail.birthDate,
      userDetail.firstName,
      userDetail.lastName,
      userDetail.updatedAt,
    );
  }
}
