import { IsDate, IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserDetailCreateDto {
  @ApiProperty({
    description: "User's first name",
    required: false,
  })
  @IsOptional()
  @IsString()
  firstName?: string;

  @ApiProperty({
    description: "User's last name",
    required: false,
  })
  @IsOptional()
  @IsString()
  lastName?: string;

  @ApiProperty({
    description: "User's birth date",
    required: false,
  })
  @IsOptional()
  @IsDate()
  birthDate?: Date;
}
