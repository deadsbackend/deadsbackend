import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';
import { UserCredentialErrorMessages } from '../types/userCredentialErrorMessages';
import { ApiProperty } from '@nestjs/swagger';

export class UserCredentialsCreateDto {
  @ApiProperty({
    description: 'E-mail address for new UserCredential',
  })
  @IsNotEmpty()
  @IsEmail({}, { message: UserCredentialErrorMessages.INVALID_EMAIL })
  email: string;

  @ApiProperty({
    description: 'Password for new UserCredential',
    minLength: 8,
    maxLength: 64,
  })
  @IsNotEmpty()
  @IsString()
  @Length(8, 64, { message: UserCredentialErrorMessages.INVALID_PASSWORD_LENGTH })
  password: string;
}
