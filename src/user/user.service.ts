import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { Prisma } from '@prisma/client';
import { v4 as uuidv4 } from 'uuid';
import { UserRole } from './types';
import { UserCredentialsResponseDto } from './dto/UserCredentials.response.dto';
import { UserUniqueAttrsRequestDto } from './dto/UserUniqueAttrs.request.dto';
import { UserCredentialsCreateDto } from './dto/UserCredentials.create.dto';
import { UserDetailResponseDto } from './dto/UserDetail.response.dto';
import { UserDetailUpdateDto } from './dto/UserDetail.update.dto';
import { PasswordService } from '../auth/password.service';
import { UserCredentialErrorMessages } from './types/userCredentialErrorMessages';

@Injectable()
export class UserService {
  private readonly logger = new Logger(UserService.name);

  constructor(private prismaService: PrismaService, private passwordService: PasswordService) {}

  async createUserCredentials(userCredentials: UserCredentialsCreateDto): Promise<UserCredentialsResponseDto> {
    const userId: string = uuidv4();
    const userPassword: string = await this.passwordService.hashPassword(userCredentials.password);
    try {
      const newUserCredential = await this.prismaService.userCredential.create({
        data: {
          userId,
          email: userCredentials.email,
          password: userPassword,
          role: UserRole.USER,
        },
      });

      return UserCredentialsResponseDto.serialize(newUserCredential);
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        if (e.code === 'P2002') {
          this.logger.error(`Error creating duplicate user credential: ${JSON.stringify(userCredentials.email)}`);
          throw new HttpException(
            { message: UserCredentialErrorMessages.DUPLICATE_USER_CREDENTIAL },
            HttpStatus.CONFLICT,
          );
        }
      }
    }

    this.logger.error(`Unknown server error.`);
    throw new HttpException(
      { message: UserCredentialErrorMessages.ERROR_CREATING_USER_CREDENTIAL },
      HttpStatus.INTERNAL_SERVER_ERROR,
    );
  }

  async getAllUsersCredentials(): Promise<UserCredentialsResponseDto[]> {
    const responseData = await this.prismaService.userCredential.findMany();
    const userCredentials: UserCredentialsResponseDto[] = [];

    responseData.map((user) => {
      userCredentials.push(UserCredentialsResponseDto.serialize(user));
    });
    return userCredentials;
  }

  async getUniqueUserCredential(attrs: UserUniqueAttrsRequestDto): Promise<UserCredentialsResponseDto> {
    const unique = attrs.userId ? { userId: attrs.userId } : { email: attrs.email };

    const user = await this.prismaService.userCredential.findUnique({
      where: unique,
    });

    if (user) {
      return UserCredentialsResponseDto.serialize(user);
    }

    throw new HttpException({ message: UserCredentialErrorMessages.USER_CREDENTIAL_NOT_FOUND }, HttpStatus.NOT_FOUND);
  }

  async getUserDetail(userId: string): Promise<UserDetailResponseDto> {
    const userDetail = await this.prismaService.userDetail.findMany({
      where: {
        user: {
          is: {
            userId,
          },
        },
      },
    });

    if (userDetail) {
      return UserDetailResponseDto.serialize(userDetail[0]);
    }

    throw new HttpException({ message: UserCredentialErrorMessages.USER_DETAIL_NOT_FOUND }, HttpStatus.NOT_FOUND);
  }

  async createOrUpdateUserDetail(userId: string, userDetail: UserDetailUpdateDto): Promise<UserDetailResponseDto> {
    const now = new Date(Date.now());
    const detailId = uuidv4();
    const userCredential = await this.prismaService.userCredential.update({
      where: {
        userId,
      },
      data: {
        detail: {
          upsert: {
            create: {
              detailId,
              updatedAt: now,
              ...userDetail,
            },
            update: {
              ...userDetail,
              updatedAt: now,
            },
          },
        },
      },
      include: {
        detail: true,
      },
    });

    if (userCredential && userCredential.detail) {
      return UserDetailResponseDto.serialize(userCredential.detail);
    } else {
      throw new HttpException({ message: UserCredentialErrorMessages.USER_CREDENTIAL_NOT_FOUND }, HttpStatus.NOT_FOUND);
    }
  }
}
