import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { PrismaModule } from '../prisma.module';
import { PasswordService } from '../auth/password.service';

@Module({
  imports: [PrismaModule],
  providers: [UserService, PasswordService],
  controllers: [UserController],
})
export class UserModule {}
