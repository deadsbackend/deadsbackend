import { userCredentialStub } from '../tests/stubs/UserCredential.stub';
import { UserCredentialsResponseDto } from '../dto/UserCredentials.response.dto';
import { UserDetailResponseDto } from '../dto/UserDetail.response.dto';
import { userDetailStub } from '../tests/stubs/UserDetail.stub';
import { UserDetailUpdateDto } from '../dto/UserDetail.update.dto';
import { HttpException, HttpStatus } from '@nestjs/common';
import { UserCredentialErrorMessages } from '../types/userCredentialErrorMessages';

export const UserService = jest.fn().mockReturnValue({
  getAllUsersCredentials: jest.fn().mockImplementation(() => {
    return userCredentialStub().map((user) => {
      UserCredentialsResponseDto.serialize(user);
    });
  }),
  getUniqueUserCredential: jest.fn().mockResolvedValue(UserCredentialsResponseDto.serialize(userCredentialStub()[0])),
  createUserCredentials: jest.fn().mockResolvedValue(UserCredentialsResponseDto.serialize(userCredentialStub()[0])),
  getUserDetail: jest.fn().mockResolvedValue(UserDetailResponseDto.serialize(userDetailStub())),
  /* eslint-disable @typescript-eslint/no-unused-vars */
  createOrUpdateUserDetail: jest
    .fn()
    .mockImplementation((userId: string, userDetail: UserDetailUpdateDto): UserDetailResponseDto | null => {
      if (userId === userCredentialStub()[0].userId) {
        return UserDetailResponseDto.serialize(userDetailStub());
      }
      throw new HttpException({ message: UserCredentialErrorMessages.USER_CREDENTIAL_NOT_FOUND }, HttpStatus.NOT_FOUND);
    }),
  /* eslint-enable @typescript-eslint/no-unused-vars */
});
