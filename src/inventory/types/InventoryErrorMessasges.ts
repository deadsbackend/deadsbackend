export enum InventoryErrorMessasges {
  INVENTORY_UPDATE_ERROR = 'Error updating inventory',
  UNKNOWN_INVENTORY_ERROR = 'Unknown server error while updating inventory',
}
