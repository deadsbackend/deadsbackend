import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsPositive, IsUUID } from 'class-validator';

export class InventoryUpdateDto {
  @ApiProperty({
    description: 'Item ID for inventory update',
  })
  @IsNotEmpty()
  @IsUUID()
  itemId: string;

  @ApiProperty({
    description: 'Quantity of items in inventory',
  })
  @IsNotEmpty()
  @IsNumber()
  @IsPositive()
  quantity: number;
}
