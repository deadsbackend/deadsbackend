import { ApiProperty } from '@nestjs/swagger';
import { Inventory } from '@prisma/client';

export class InventoryResponseDto {
  @ApiProperty({
    description: 'Item ID (UUIDv4)',
  })
  itemId: string;

  @ApiProperty({
    description: 'Current item quantity in inventory',
  })
  quantity: number;

  @ApiProperty({
    description: 'Last inventory update time',
  })
  updatedAt: Date;

  static serialize(inventory: Inventory): InventoryResponseDto {
    return {
      itemId: inventory.inventoryId,
      quantity: inventory.quantity,
      updatedAt: inventory.updatedAt,
    };
  }
}
