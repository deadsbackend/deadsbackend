import { Body, Controller, Post } from '@nestjs/common';
import { InventoryService } from './inventory.service';
import { ApiBearerAuth, ApiCreatedResponse, ApiInternalServerErrorResponse, ApiTags } from '@nestjs/swagger';
import { InventoryUpdateDto } from './dto/inventory.update.dto';
import { InventoryResponseDto } from './dto/inventory.response.dto';

@ApiTags('Inventory')
@Controller('/inventory')
export class InventoryController {
  constructor(private inventoryService: InventoryService) {}

  @Post()
  @ApiCreatedResponse({
    description: 'Created or updated inventory record',
    type: InventoryResponseDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Error while creating or updating inventory record',
  })
  @ApiBearerAuth()
  async updateInventory(@Body() inventoryUpdateDto: InventoryUpdateDto): Promise<InventoryResponseDto> {
    return this.inventoryService.updateInventory(inventoryUpdateDto);
  }
}
