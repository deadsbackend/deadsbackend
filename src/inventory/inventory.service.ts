import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { v4 as uuidv4 } from 'uuid';
import { InventoryUpdateDto } from './dto/inventory.update.dto';
import { InventoryResponseDto } from './dto/inventory.response.dto';
import { Prisma } from '@prisma/client';
import { InventoryErrorMessasges } from './types/InventoryErrorMessasges';

@Injectable()
export class InventoryService {
  private readonly logger = new Logger(InventoryService.name);

  constructor(private prismaService: PrismaService) {}

  async updateInventory(inventoryUpdateDto: InventoryUpdateDto): Promise<InventoryResponseDto> {
    const newUUID = uuidv4();
    try {
      await this.prismaService.item.update({
        where: {
          itemId: inventoryUpdateDto.itemId,
        },
        data: {
          inventory: {
            upsert: {
              create: {
                inventoryId: newUUID,
                quantity: inventoryUpdateDto.quantity,
              },
              update: {
                quantity: inventoryUpdateDto.quantity,
              },
            },
          },
        },
      });
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        this.logger.debug(`Caught exception with Prisma error code ${e.code}`);
        if (e.code === 'P2002') {
          this.logger.error(`Error updating inventory for item: ${JSON.stringify(inventoryUpdateDto.itemId)}`);
          throw new HttpException(
            { message: InventoryErrorMessasges.INVENTORY_UPDATE_ERROR },
            HttpStatus.INTERNAL_SERVER_ERROR,
          );
        }
      }
    }
    const updatedInventory = await this.prismaService.item
      .findUnique({
        where: {
          itemId: inventoryUpdateDto.itemId,
        },
      })
      .inventory();

    if (updatedInventory) {
      return InventoryResponseDto.serialize(updatedInventory);
    }

    throw new HttpException(
      { message: InventoryErrorMessasges.UNKNOWN_INVENTORY_ERROR },
      HttpStatus.INTERNAL_SERVER_ERROR,
    );
  }
}
