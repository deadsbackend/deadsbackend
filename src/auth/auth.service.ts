import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { TokenService } from './token.service';
import { UserCredential } from '@prisma/client';
import { PrismaService } from '../prisma.service';
import { AuthErrorMessages } from './types/authErrorMessages';
import { UserLoginRequestDto } from './dto/UserLogin.request.dto';
import { UserLoginResponseDto } from './dto/UserLogin.response.dto';
import { UserRole } from '../user/types';
import { PasswordService } from './password.service';

@Injectable()
export class AuthService {
  private readonly logger = new Logger(AuthService.name);

  constructor(
    private prismaService: PrismaService,
    private tokenService: TokenService,
    private passwordService: PasswordService,
  ) {}

  async loginUser(userCredentials: UserLoginRequestDto): Promise<UserLoginResponseDto> {
    const user = await this.validateUser(userCredentials);

    const tokens = this.tokenService.generateTokens({
      userId: user.userId,
      email: user.email,
      userRole: user.role as UserRole,
    });

    const userCredential = await this.tokenService.saveRefreshToken(user, tokens.refreshToken);

    return UserLoginResponseDto.serialize(userCredential, tokens);
  }

  async validateUser(userCredentials: UserLoginRequestDto): Promise<UserCredential> {
    const user = await this.prismaService.userCredential.findUnique({
      where: {
        email: userCredentials.email,
      },
    });

    if (user) {
      const isPasswordValid = await this.passwordService.isPasswordValid(userCredentials.password, user.password);

      if (isPasswordValid) {
        return user;
      } else {
        this.logger.log(`Unauthorized login attempt for ${userCredentials.email}`);
      }
    }
    throw new UnauthorizedException({ message: AuthErrorMessages.UNATHORIZED_LOGIN_ATTEMPT });
  }

  async refreshUserTokens(refreshToken: string): Promise<UserLoginResponseDto> {
    if (!refreshToken) {
      this.logger.error(AuthErrorMessages.INVALID_REFRESH_TOKEN);
      throw new UnauthorizedException({ message: AuthErrorMessages.INVALID_REFRESH_TOKEN });
    }
    this.logger.debug(`Starting tokens refresh...`);

    const userData = this.tokenService.validateRefreshToken(refreshToken);
    if (!userData) {
      this.logger.error(AuthErrorMessages.INVALID_REFRESH_TOKEN);
      throw new UnauthorizedException({ message: AuthErrorMessages.INVALID_REFRESH_TOKEN });
    }
    this.logger.debug(`Got user data from token: ${JSON.stringify(userData)}`);

    const userCredentials = await this.prismaService.userCredential.findUnique({
      where: {
        userId: userData.userId,
      },
    });
    if (!userCredentials) {
      this.logger.error(AuthErrorMessages.INVALID_REFRESH_TOKEN_PAYLOAD);
      throw new UnauthorizedException({ message: AuthErrorMessages.INVALID_REFRESH_TOKEN_PAYLOAD });
    }

    const tokens = this.tokenService.generateTokens({
      userId: userData.userId,
      userRole: userData.userRole,
      email: userData.email,
    });
    await this.tokenService.saveRefreshToken(userCredentials, tokens.refreshToken);
    return UserLoginResponseDto.serialize(userCredentials, tokens);
  }

  async logoutUser(userId: string): Promise<void> {
    await this.prismaService.userCredential.update({
      where: {
        userId,
      },
      data: {
        refreshToken: '',
      },
    });
  }
}
