import { Global, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TokenService } from './token.service';
import { ApiConfigService } from '../config/apiConfig.service';
import { EnvVariables } from '../config/config.types';
import { PrismaModule } from '../prisma.module';
import { ConfigModule } from '@nestjs/config';

@Global()
@Module({
  imports: [
    JwtModule.register({
      secret: process.env[EnvVariables.JWT_ACCESS_SECRET],
    }),
    PrismaModule,
    ConfigModule,
  ],
  controllers: [],
  providers: [TokenService, ApiConfigService],
  exports: [TokenService],
})
export class TokenModule {}
