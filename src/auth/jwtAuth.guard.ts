import { CanActivate, ExecutionContext, Injectable, Logger, SetMetadata, UnauthorizedException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AuthErrorMessages } from './types/authErrorMessages';
import { TokenService } from './token.service';
import { Reflector } from '@nestjs/core';
import { AuthorizedRequest } from './types/authorizedRequest.type';

export const Public = () => SetMetadata('isPublic', true);

@Injectable()
export class JwtAuthGuard implements CanActivate {
  private logger = new Logger(JwtAuthGuard.name);

  constructor(private tokenService: TokenService, private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const isPublic = this.reflector.get<boolean>('isPublic', context.getHandler());
    if (isPublic) {
      return true;
    }

    const req = context.switchToHttp().getRequest<AuthorizedRequest>();

    if (req) {
      const authHeader = req.headers.authorization;
      this.logger.debug(`Checking auth header.`);

      if (!authHeader) {
        this.logger.error(`Auth error: ${AuthErrorMessages.NO_AUTH_HEADER}`);
        throw new UnauthorizedException({ message: AuthErrorMessages.NO_AUTH_HEADER });
      }

      this.logger.debug(`Checking bearer auth.`);
      const bearer = authHeader.split(' ')[0];
      const token = authHeader.split(' ')[1];
      if (bearer !== 'Bearer' || !token) {
        this.logger.error(`Auth error: ${AuthErrorMessages.INVALID_AUTH_HEADER}`);
        throw new UnauthorizedException({ message: AuthErrorMessages.INVALID_AUTH_HEADER });
      }

      req.user = this.tokenService.validateAccessToken(token);
      if (req.user) {
        this.logger.debug(`Setting req.user to ${JSON.stringify(req.user)}`);
        return true;
      } else {
        this.logger.error(`Auth error: ${AuthErrorMessages.INVALID_ACCESS_TOKEN}`);
        throw new UnauthorizedException({ message: AuthErrorMessages.INVALID_ACCESS_TOKEN });
      }
    }

    return false;
  }
}
