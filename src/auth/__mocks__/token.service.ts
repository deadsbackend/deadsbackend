import { invalidAccessTokenPayload, invalidRefreshTokenPayload, userTokensStub } from '../tests/stubs/UserTokens.stub';
import { userCredentialStub } from '../../user/tests/stubs/UserCredential.stub';
import { invalidTokenPayloadStub, tokenPayloadStub } from '../tests/stubs/TokenPayload.stub';

export const TokenService = jest.fn().mockReturnValue({
  generateTokens: jest.fn().mockReturnValue(userTokensStub()),
  saveRefreshToken: jest.fn().mockResolvedValue(userCredentialStub()[0]),
  validateRefreshToken: jest.fn().mockImplementation((token: string) => {
    if (token === 'invalid-token') return null;
    if (token === invalidRefreshTokenPayload()) return invalidTokenPayloadStub();
    return tokenPayloadStub();
  }),
  validateAccessToken: jest.fn().mockImplementation((token: string) => {
    if (token === 'invalid-token') return null;
    if (token === invalidAccessTokenPayload()) return null;
    return tokenPayloadStub();
  }),
});
