import { userLoginResponseStub } from '../tests/stubs/UserLoginResponse.stub';

export const AuthService = jest.fn().mockReturnValue({
  loginUser: jest.fn().mockResolvedValue(userLoginResponseStub()),
  refreshUserTokens: jest.fn().mockResolvedValue(userLoginResponseStub()),
  logoutUser: jest.fn(),
});
