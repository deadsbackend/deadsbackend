export const PasswordService = jest.fn().mockReturnValue({
  isPasswordValid: jest.fn().mockImplementation((password: string, hash: string): boolean => {
    return password === hash;
  }),
  hashPassword: jest.fn().mockImplementation((password: string) => password),
});
