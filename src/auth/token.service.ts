import { Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { PrismaService } from '../prisma.service';
import { UserTokenPayload, UserTokens } from './types/tokens.type';
import { ApiConfigService } from '../config/apiConfig.service';
import { UserCredential } from '@prisma/client';

@Injectable()
export class TokenService {
  private readonly logger = new Logger(TokenService.name);

  constructor(
    private jwtService: JwtService,
    private prismaService: PrismaService,
    private apiConfigService: ApiConfigService,
  ) {}

  generateTokens(tokenPayload: UserTokenPayload): UserTokens {
    const accessToken = this.jwtService.sign(tokenPayload, {
      expiresIn: '1d',
      secret: this.apiConfigService.jwtAccessSecret,
    });

    const refreshToken = this.jwtService.sign(tokenPayload, {
      expiresIn: '30d',
      secret: this.apiConfigService.jwtRefreshSecret,
    });

    return {
      accessToken,
      refreshToken,
    };
  }

  async saveRefreshToken(user: UserCredential, token: string): Promise<UserCredential> {
    return this.prismaService.userCredential.update({
      where: {
        id: user.id,
      },
      data: {
        refreshToken: token,
        updatedAt: new Date(Date.now()),
      },
    });
  }

  async removeRefreshToken(user: UserCredential): Promise<void> {
    await this.prismaService.userCredential.update({
      where: {
        id: user.id,
      },
      data: {
        refreshToken: '',
      },
    });
  }

  validateAccessToken(token: string): UserTokenPayload | null {
    try {
      return this.jwtService.verify<UserTokenPayload>(token, {
        secret: this.apiConfigService.jwtAccessSecret,
      });
    } catch (e) {
      this.logger.error(`Cannot verify access token ${token}`);
      return null;
    }
  }

  validateRefreshToken(token: string): UserTokenPayload | null {
    try {
      return this.jwtService.verify<UserTokenPayload>(token, {
        secret: this.apiConfigService.jwtRefreshSecret,
      });
    } catch (e) {
      this.logger.error(`Cannot verify refresh token ${token}`);
      return null;
    }
  }
}
