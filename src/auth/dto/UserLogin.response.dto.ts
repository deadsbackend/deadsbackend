import { UserCredentialsResponseDto } from '../../user/dto/UserCredentials.response.dto';
import { UserTokens } from '../types/tokens.type';
import { UserCredential } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';

export class UserLoginResponseDto {
  constructor(userId: string, userCredentials: UserCredentialsResponseDto, userTokens: UserTokens) {
    this.userId = userId;
    this.userCredentials = userCredentials;
    this.userTokens = userTokens;
  }

  @ApiProperty({
    description: 'Logged in user ID (UUIDv4)',
  })
  userId: string;

  @ApiProperty({
    description: 'Logged in user credentials',
    type: [UserCredentialsResponseDto],
  })
  userCredentials: UserCredentialsResponseDto;

  @ApiProperty({
    description: 'Access & refresh tokens for further use',
    type: [UserTokens],
  })
  userTokens: UserTokens;

  static serialize(user: UserCredential, userTokens: UserTokens): UserLoginResponseDto {
    return new UserLoginResponseDto(user.userId, UserCredentialsResponseDto.serialize(user), userTokens);
  }
}
