import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';
import { UserCredentialErrorMessages } from '../../user/types/userCredentialErrorMessages';

export class UserLoginRequestDto {
  @ApiProperty({
    description: 'E-mail address for login',
  })
  @IsNotEmpty()
  @IsEmail({}, { message: UserCredentialErrorMessages.INVALID_EMAIL })
  email: string;

  @ApiProperty({
    description: 'Password for login',
    minLength: 8,
    maxLength: 64,
  })
  @IsNotEmpty()
  @IsString()
  @Length(8, 64, { message: UserCredentialErrorMessages.INVALID_PASSWORD_LENGTH })
  password: string;
}
