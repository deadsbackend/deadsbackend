import { UserRole } from '../../user/types';
import { ApiProperty } from '@nestjs/swagger';

export class UserTokens {
  @ApiProperty({
    description: 'JWT access token for API access',
  })
  accessToken: string;

  @ApiProperty({
    description: 'JWT refresh token to refresh expired API access token',
  })
  refreshToken: string;
}

export interface UserTokenPayload {
  userId: string;
  email: string;
  userRole: UserRole;
}
