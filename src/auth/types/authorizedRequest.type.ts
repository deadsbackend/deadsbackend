import { Request } from 'express';
import { UserTokenPayload } from './tokens.type';

export interface AuthorizedRequest extends Request {
  user: UserTokenPayload | null;
}
