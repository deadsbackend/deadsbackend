export interface Cookies {
  refreshToken: string;

  [key: string]: string;
}
