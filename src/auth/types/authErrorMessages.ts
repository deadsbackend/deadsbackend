export enum AuthErrorMessages {
  UNATHORIZED_LOGIN_ATTEMPT = 'Unauthorized login attempt',
  NO_AUTH_HEADER = 'No authorization header set',
  INVALID_AUTH_HEADER = 'Invalid authorization header',
  INVALID_ACCESS_TOKEN = 'Invalid access token',
  INVALID_REFRESH_TOKEN = 'Invalid refresh token',
  INVALID_REFRESH_TOKEN_PAYLOAD = 'Invalid refresh token payload',
}
