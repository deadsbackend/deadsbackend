import { JwtAuthGuard } from '../jwtAuth.guard';
import { createMock } from '@golevelup/ts-jest';
import { ExecutionContext } from '@nestjs/common';
import { invalidAccessTokenPayload, userTokensStub } from './stubs/UserTokens.stub';
import { Reflector } from '@nestjs/core';
import { TokenService } from '../token.service';
import { Test } from '@nestjs/testing';
import { AuthErrorMessages } from '../types/authErrorMessages';
import { DisabledLogger } from '../../lib/logger/DisabledLogger';

const noAuthHeaderContext = createMock<ExecutionContext>();
const invalidAuthHeaderContext = createMock<ExecutionContext>({
  switchToHttp: () => ({
    getRequest: () => ({
      headers: {
        authorization: `${userTokensStub().accessToken}`,
      },
    }),
  }),
});
const invalidTokenPayloadContext = createMock<ExecutionContext>({
  switchToHttp: () => ({
    getRequest: () => ({
      headers: {
        authorization: `Bearer ${invalidAccessTokenPayload()}`,
      },
    }),
  }),
});
const authorizedContext = createMock<ExecutionContext>({
  switchToHttp: () => ({
    getRequest: () => ({
      headers: {
        authorization: `Bearer ${userTokensStub().accessToken}`,
      },
    }),
  }),
});

const reflector = createMock<Reflector>();

jest.mock('../token.service');

describe('jwtAuthGuard', () => {
  let jwtAuthGuard: JwtAuthGuard;
  let tokenService: TokenService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [TokenService],
    }).compile();
    moduleRef.useLogger(new DisabledLogger());

    tokenService = moduleRef.get<TokenService>(TokenService);
  });
  describe('public routes', () => {
    beforeEach(() => {
      jwtAuthGuard = new JwtAuthGuard(tokenService, reflector);
      reflector.get.mockReturnValue(true);
    });

    test('it should return true for auth header with correct token', () => {
      expect(jwtAuthGuard.canActivate(authorizedContext)).toBeTruthy();
    });
    test('it should return true with no auth header', () => {
      expect(jwtAuthGuard.canActivate(noAuthHeaderContext)).toBeTruthy();
    });
    test('it should return true with invalid auth header', () => {
      expect(jwtAuthGuard.canActivate(invalidAuthHeaderContext)).toBeTruthy();
    });
  });

  describe('private routes', () => {
    beforeEach(() => {
      jwtAuthGuard = new JwtAuthGuard(tokenService, reflector);
      reflector.get.mockReturnValue(false);
    });

    test('it should return true for auth header with correct token', () => {
      expect(jwtAuthGuard.canActivate(authorizedContext)).toBeTruthy();
    });
    test('it should trow UnauthorizedException if no auth header is present', () => {
      expect(() => jwtAuthGuard.canActivate(noAuthHeaderContext)).toThrow(AuthErrorMessages.NO_AUTH_HEADER);
    });
    test('it should throw UnauthorizedException if auth header is invalid', () => {
      expect(() => jwtAuthGuard.canActivate(invalidAuthHeaderContext)).toThrow(AuthErrorMessages.INVALID_AUTH_HEADER);
    });
    test('it should throw new UnauthorizedException if access token is invalid', () => {
      expect(() => jwtAuthGuard.canActivate(invalidTokenPayloadContext)).toThrow(
        AuthErrorMessages.INVALID_ACCESS_TOKEN,
      );
    });
  });
});
