import { Test } from '@nestjs/testing';
import { PrismaModule } from '../../prisma.module';
import { TokenModule } from '../token.module';
import { AuthService } from '../auth.service';
import { PasswordService } from '../password.service';
import { invalidPasswordUserLoginRequestStub, userLoginRequestStub } from './stubs/UserLoginRequest.stub';
import { userCredentialStub } from '../../user/tests/stubs/UserCredential.stub';
import { AuthErrorMessages } from '../types/authErrorMessages';
import { UserLoginResponseDto } from '../dto/UserLogin.response.dto';
import { invalidRefreshTokenPayload, userTokensStub } from './stubs/UserTokens.stub';
import { DisabledLogger } from '../../lib/logger/DisabledLogger';

jest.mock('../password.service');
jest.mock('../token.service');
jest.mock('../../prisma.service');

describe('AuthService', () => {
  let authService: AuthService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [PrismaModule, TokenModule],
      providers: [AuthService, PasswordService],
    }).compile();
    moduleRef.useLogger(new DisabledLogger());

    authService = moduleRef.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(authService).toBeDefined();
  });

  describe('validateUser', () => {
    describe('with valid credentials', () => {
      test('it should validate and return user credentials', async () => {
        const user = await authService.validateUser(userLoginRequestStub());
        expect(user).toBeDefined();
        expect(user.userId).toEqual(userCredentialStub()[0].userId);
      });
    });

    describe('with invalid credentials', () => {
      test('it should throw UnauthorizedException', async () => {
        await expect(authService.validateUser(invalidPasswordUserLoginRequestStub())).rejects.toThrow(
          AuthErrorMessages.UNATHORIZED_LOGIN_ATTEMPT,
        );
      });
    });
  });

  describe('loginUser', () => {
    describe('with valid credentials', () => {
      test('it should return user credential', async () => {
        const user = await authService.loginUser(userLoginRequestStub());
        expect(user).toBeDefined();
        expect(user).toBeInstanceOf(UserLoginResponseDto);
        expect(user.userId).toEqual(userCredentialStub()[0].userId);
      });
    });

    describe('with invalid credentials', () => {
      test('it should throw UnauthorizedException', async () => {
        await expect(authService.validateUser(invalidPasswordUserLoginRequestStub())).rejects.toThrow(
          AuthErrorMessages.UNATHORIZED_LOGIN_ATTEMPT,
        );
      });
    });
  });

  describe('refreshUserTokens', () => {
    describe('with valid token', () => {
      let user: UserLoginResponseDto;

      beforeEach(async () => {
        user = await authService.refreshUserTokens(userTokensStub().refreshToken);
      });

      test('it should return user credentials', () => {
        expect(user).toBeDefined();
        expect(user).toBeInstanceOf(UserLoginResponseDto);
        expect(user.userId).toEqual(userCredentialStub()[0].userId);
      });
    });

    describe('with invalid token', () => {
      beforeEach(() => {
        jest.clearAllMocks();
      });

      test('with empty token it should throw UnauthorizedException', async () => {
        await expect(authService.refreshUserTokens('')).rejects.toThrow(AuthErrorMessages.INVALID_REFRESH_TOKEN);
      });

      test('with invalid token format', async () => {
        await expect(authService.refreshUserTokens('invalid-token')).rejects.toThrow(
          AuthErrorMessages.INVALID_REFRESH_TOKEN,
        );
      });

      test('with invalid token payload', async () => {
        await expect(authService.refreshUserTokens(invalidRefreshTokenPayload())).rejects.toThrow(
          AuthErrorMessages.INVALID_REFRESH_TOKEN_PAYLOAD,
        );
      });
    });
  });
});
