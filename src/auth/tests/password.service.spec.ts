import { Test } from '@nestjs/testing';
import { PasswordService } from '../password.service';
import { userLoginRequestStub } from './stubs/UserLoginRequest.stub';
import { DisabledLogger } from '../../lib/logger/DisabledLogger';

describe('PasswordService', () => {
  let passwordService: PasswordService;
  let passwordHash: string;
  let isPasswordValid: boolean;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [PasswordService],
    }).compile();
    moduleRef.useLogger(new DisabledLogger());

    passwordService = moduleRef.get<PasswordService>(PasswordService);
  });

  it('it should be defined', () => {
    expect(passwordService).toBeDefined();
  });

  describe('with provided correct password', () => {
    beforeEach(async () => {
      passwordHash = await passwordService.hashPassword(userLoginRequestStub().password);
      isPasswordValid = await passwordService.isPasswordValid(userLoginRequestStub().password, passwordHash);
    });

    test('it should return password hash', () => {
      expect(passwordHash).not.toEqual(undefined);
    });

    test('it should verify same password against received hash', () => {
      expect(isPasswordValid).toEqual(true);
    });
  });

  describe('with provided incorrect password', () => {
    beforeEach(async () => {
      passwordHash = await passwordService.hashPassword(userLoginRequestStub().password);
      isPasswordValid = await passwordService.isPasswordValid('wrong-password', passwordHash);
    });

    test('it should not verify', () => {
      expect(isPasswordValid).toEqual(false);
    });
  });
});
