import { Test } from '@nestjs/testing';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { EnvVariables } from '../../config/config.types';
import { PrismaModule } from '../../prisma.module';
import { ConfigModule } from '@nestjs/config';
import { TokenService } from '../token.service';
import { ApiConfigService } from '../../config/apiConfig.service';
import { userCredentialStub } from '../../user/tests/stubs/UserCredential.stub';
import { UserTokenPayload } from '../types/tokens.type';
import { tokenPayloadStub } from './stubs/TokenPayload.stub';
import { userTokensStub } from './stubs/UserTokens.stub';
import { UserCredential } from '@prisma/client';
import { PrismaService } from '../../prisma.service';
import { DisabledLogger } from '../../lib/logger/DisabledLogger';

jest.mock('../../prisma.service');

describe('TokenService', () => {
  let tokenService: TokenService;
  let jwtService: JwtService;
  let apiConfigService: ApiConfigService;
  let prismaService: PrismaService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        JwtModule.register({
          secret: process.env[EnvVariables.JWT_ACCESS_SECRET],
        }),
        PrismaModule,
        ConfigModule,
      ],
      providers: [TokenService, ApiConfigService],
    }).compile();
    moduleRef.useLogger(new DisabledLogger());

    tokenService = moduleRef.get<TokenService>(TokenService);
    jwtService = moduleRef.get<JwtService>(JwtService);
    apiConfigService = moduleRef.get<ApiConfigService>(ApiConfigService);
    prismaService = moduleRef.get<PrismaService>(PrismaService);
  });

  it('should be defined', () => {
    expect(tokenService).toBeDefined();
  });

  describe('generateTokens', () => {
    test('should return user tokens', () => {
      const generatedTokens = tokenService.generateTokens(tokenPayloadStub());
      expect(
        jwtService.verify<UserTokenPayload>(generatedTokens.accessToken, {
          secret: apiConfigService.jwtAccessSecret,
        }).userId,
      ).toEqual(tokenPayloadStub().userId);
      expect(
        jwtService.verify<UserTokenPayload>(generatedTokens.refreshToken, {
          secret: apiConfigService.jwtRefreshSecret,
        }).userId,
      ).toEqual(tokenPayloadStub().userId);
    });
  });

  describe('saveRefreshToken', () => {
    let updatedUser: UserCredential;

    beforeEach(async () => {
      updatedUser = await tokenService.saveRefreshToken(userCredentialStub()[0], userTokensStub().refreshToken);
    });

    test('should return updated userCredential', () => {
      expect(updatedUser.userId).toEqual(userCredentialStub()[0].userId);
    });

    test('update should be called with userId and token', () => {
      expect(prismaService.userCredential.update).toHaveBeenCalledWith(
        expect.objectContaining({
          // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
          where: expect.objectContaining({
            id: updatedUser.id,
          }),
          // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
          data: expect.objectContaining({
            refreshToken: userTokensStub().refreshToken,
          }),
        }),
      );
    });
  });

  describe('removeRefreshToken', () => {
    beforeEach(async () => {
      await tokenService.removeRefreshToken(userCredentialStub()[0]);
    });

    test('update should be called with userId and empty token', () => {
      expect(prismaService.userCredential.update).toHaveBeenCalledWith(
        expect.objectContaining({
          // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
          where: expect.objectContaining({
            id: userCredentialStub()[0].id,
          }),
          // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
          data: expect.objectContaining({
            refreshToken: '',
          }),
        }),
      );
    });
  });

  describe('validateAccessToken', () => {
    test('should return UserTokenPayload when valid token supplied', () => {
      const userTokenPayload = tokenService.validateAccessToken(userTokensStub().accessToken);
      expect(userTokenPayload).toEqual(expect.objectContaining({ ...tokenPayloadStub() }));
    });

    test('should return null if invalid token supplied', () => {
      const userTokenPayload = tokenService.validateAccessToken('invalid-token');
      expect(userTokenPayload).toEqual(null);
    });
  });

  describe('validateRefreshToken', () => {
    test('should return UserTokenPayload when valid token supplied', () => {
      const userTokenPayload = tokenService.validateRefreshToken(userTokensStub().refreshToken);

      expect(userTokenPayload).toEqual(expect.objectContaining({ ...tokenPayloadStub() }));
    });

    test('should return null if invalid token supplied', () => {
      const userTokenPayload = tokenService.validateRefreshToken('invalid-token');

      expect(userTokenPayload).toEqual(null);
    });
  });
});
