import { Test } from '@nestjs/testing';
import { AuthController } from '../auth.controller';
import { AuthService } from '../auth.service';
import { UserLoginResponseDto } from '../dto/UserLogin.response.dto';
import { Request, Response } from 'express';
import { createMock } from '@golevelup/ts-jest';
import { userLoginRequestStub } from './stubs/UserLoginRequest.stub';
import { UserLoginRequestDto } from '../dto/UserLogin.request.dto';
import { userLoginResponseStub } from './stubs/UserLoginResponse.stub';
import { userTokensStub } from './stubs/UserTokens.stub';
import { ExecutionContext } from '@nestjs/common';
import { AuthorizedRequest } from '../types/authorizedRequest.type';
import { UserRole } from '../../user/types';
import { ConfigModule } from '@nestjs/config';
import { ApiConfigService } from '../../config/apiConfig.service';
import { DisabledLogger } from '../../lib/logger/DisabledLogger';

jest.mock('../auth.service');

const mockResponseObject = () => {
  return createMock<Response>({
    cookie: jest.fn().mockReturnThis(),
    status: jest.fn().mockReturnThis(),
  });
};

const mockRefreshTokenCookie = () => {
  return createMock<ExecutionContext>({
    switchToHttp: () => ({
      getRequest: () => ({
        cookies: {
          refreshToken: userTokensStub().refreshToken,
        },
      }),
    }),
  });
};

const mockLogoutRequest = () => {
  return createMock<ExecutionContext>({
    switchToHttp: () => ({
      getRequest: (): AuthorizedRequest =>
        ({
          headers: {
            authorization: `Bearer ${userTokensStub().accessToken}`,
          },
          user: {
            userId: userLoginResponseStub().userId,
            userRole: UserRole.USER,
            email: userLoginResponseStub().userCredentials.email,
          },
        } as AuthorizedRequest),
    }),
  });
};

describe('Auth controller', () => {
  let authController: AuthController;
  let authService: AuthService;
  let apiConfigService: ApiConfigService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [ConfigModule],
      controllers: [AuthController],
      providers: [AuthService, ApiConfigService],
    }).compile();
    moduleRef.useLogger(new DisabledLogger());

    authController = moduleRef.get<AuthController>(AuthController);
    authService = moduleRef.get<AuthService>(AuthService);
    apiConfigService = moduleRef.get<ApiConfigService>(ApiConfigService);
    jest.clearAllMocks();
  });

  describe('Login user', () => {
    let userLoginRequest: UserLoginRequestDto;
    let userLoginResponse: UserLoginResponseDto;
    let response: Response;

    beforeEach(async () => {
      response = mockResponseObject();
      userLoginRequest = userLoginRequestStub();
      userLoginResponse = await authController.loginUser(response, userLoginRequest);
    });

    describe('With correct request data', () => {
      test('it should call authService.loginUser', () => {
        expect(authService.loginUser).toHaveBeenCalledWith(userLoginRequest);
      });

      test('it should return UserLoginResponseDto object', () => {
        expect(userLoginResponse).toEqual(userLoginResponseStub());
      });

      test('it should set refreshToken cookie', () => {
        expect(response.cookie).toHaveBeenCalledWith(
          'refreshToken',
          userLoginResponse.userTokens.refreshToken,
          apiConfigService.tokenCookieOptions,
        );
      });
    });
  });

  describe('Refresh user tokens', () => {
    let refreshUserTokensResponse: UserLoginResponseDto;
    let response: Response;
    let request: Request;

    beforeEach(async () => {
      response = mockResponseObject();
      request = mockRefreshTokenCookie().switchToHttp().getRequest() as Request;
      refreshUserTokensResponse = await authController.refreshUserTokens(response, request);
    });

    describe('With correctly set refreshToken cookie', () => {
      test('it should call authService.refreshUserTokens', () => {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
        expect(authService.refreshUserTokens).toHaveBeenCalledWith(request.cookies.refreshToken);
      });

      test('it should return UserLoginResponseDto', () => {
        expect(refreshUserTokensResponse).toEqual(userLoginResponseStub());
      });

      test('it should set refreshToken cookie', () => {
        expect(response.cookie).toHaveBeenCalledWith(
          'refreshToken',
          refreshUserTokensResponse.userTokens.refreshToken,
          apiConfigService.tokenCookieOptions,
        );
      });
    });
  });

  describe('Logout user', () => {
    let response: Response;
    let request: AuthorizedRequest;

    beforeEach(async () => {
      response = mockResponseObject();
      request = mockLogoutRequest().switchToHttp().getRequest() as AuthorizedRequest;
      await authController.logoutUser(response, request);
    });

    describe('With accessToken and correct userID set', () => {
      test('it should call authService.logoutUser', () => {
        expect(authService.logoutUser).toHaveBeenCalledWith(userLoginResponseStub().userId);
      });

      test('it should clear refreshToken cookie', () => {
        expect(response.clearCookie).toHaveBeenCalledWith('refreshToken');
      });

      test('it should return 200 HTTP code', () => {
        expect(response.status).toHaveBeenCalledWith(200);
      });
    });
  });
});
