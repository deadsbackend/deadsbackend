import { UserTokens } from '../../types/tokens.type';

export const userTokensStub = (): UserTokens => {
  return {
    accessToken:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1NjA0MjgxYy1hMGFjLTQyMTktYTVhNS01MTAxYTU3MjhhZmQiLCJ1c2VyUm9sZSI6IlVTRVIiLCJlbWFpbCI6InJvb3RAaWJtLmNvbSIsImlhdCI6MTY1MDU3MTk2OCwiZXhwIjoyNTM0MzAzNTYwfQ.LdVmlNO1w0O1mVd9ULh3gCAQSeIiXkhHLqcdiPaS3N0',
    refreshToken:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1NjA0MjgxYy1hMGFjLTQyMTktYTVhNS01MTAxYTU3MjhhZmQiLCJ1c2VyUm9sZSI6IlVTRVIiLCJlbWFpbCI6InJvb3RAaWJtLmNvbSIsImlhdCI6MTY1MDU3MTk2OCwiZXhwIjoyNTM0MzAzNTYwfQ.X7cc9ynvSDibsKMWfp5sS6vtz1QX7gKNz6vI75Mnv3c',
  };
};

export const invalidRefreshTokenPayload = (): string => {
  return 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1NjA0MjgxYy1hMGFjLTQyMTktYTVhNS01MTAxYTU3Mjh4eHgiLCJ1c2VyUm9sZSI6IlVTRVIiLCJlbWFpbCI6InJvb3RAc3VuLmNvbSIsImlhdCI6MTY1MDU3MTk2OCwiZXhwIjoxNjUzMTYzOTY4fQ.WKm-AvYjnttOvCAu3MU5G9RTyI6qgozJutHp8Fha_9U';
};

export const invalidAccessTokenPayload = (): string => {
  return 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1NjA0MjgxYy1hMGFjLTQyMTktYTVhNS01MTAxYTU3Mjh4eHgiLCJ1c2VyUm9sZSI6IlVTRVIiLCJlbWFpbCI6InJvb3RAeHh4LmNvbSIsImlhdCI6MTY1MDU3MTk2OCwiZXhwIjoxNjUwNjU4MzY4fQ.2EgpfCa9tvxUC6dVpTeJ0AXvQaVNtswQ2OEUMcNlsQ4';
};
