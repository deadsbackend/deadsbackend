import { UserLoginRequestDto } from '../../dto/UserLogin.request.dto';
import { userCredentialStub } from '../../../user/tests/stubs/UserCredential.stub';

export const userLoginRequestStub = (): UserLoginRequestDto => {
  return {
    email: userCredentialStub()[0].email,
    password: userCredentialStub()[0].password,
  };
};

export const invalidEmailUserLoginRequestStub = (): UserLoginRequestDto => {
  return {
    email: 'dddddd',
    password: userCredentialStub()[0].password,
  };
};

export const invalidPasswordUserLoginRequestStub = (): UserLoginRequestDto => {
  return {
    email: userCredentialStub()[0].email,
    password: 'very',
  };
};
