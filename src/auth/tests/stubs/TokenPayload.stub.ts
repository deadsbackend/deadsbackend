import { UserTokenPayload } from '../../types/tokens.type';
import { invalidUserCredentialStub, userCredentialStub } from '../../../user/tests/stubs/UserCredential.stub';
import { UserRole } from '../../../user/types';

export const tokenPayloadStub = (): UserTokenPayload => {
  return {
    userId: userCredentialStub()[0].userId,
    email: userCredentialStub()[0].email,
    userRole: UserRole.USER,
  };
};

export const invalidTokenPayloadStub = (): UserTokenPayload => {
  return {
    userId: invalidUserCredentialStub().userId,
    email: invalidUserCredentialStub().email,
    userRole: UserRole.USER,
  };
};
