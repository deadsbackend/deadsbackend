import { UserLoginResponseDto } from '../../dto/UserLogin.response.dto';
import { userTokensStub } from './UserTokens.stub';
import { userCredentialStub } from '../../../user/tests/stubs/UserCredential.stub';

export const userLoginResponseStub = (): UserLoginResponseDto => {
  return {
    userId: userCredentialStub()[0].userId,
    userTokens: userTokensStub(),
    userCredentials: {
      userId: userCredentialStub()[0].userId,
      email: userCredentialStub()[0].email,
      updatedAt: userCredentialStub()[0].updatedAt,
    },
  };
};
