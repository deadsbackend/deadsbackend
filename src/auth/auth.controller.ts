import { Body, Controller, Get, HttpStatus, Logger, Post, Req, Res } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserLoginRequestDto } from './dto/UserLogin.request.dto';
import { UserLoginResponseDto } from './dto/UserLogin.response.dto';
import { Public } from './jwtAuth.guard';
import { Request, Response } from 'express';
import { Cookies } from './types/cookies.type';
import { AuthorizedRequest } from './types/authorizedRequest.type';
import { ApiBearerAuth, ApiCookieAuth, ApiOkResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { ApiConfigService } from '../config/apiConfig.service';

@ApiTags('Authentication')
@Controller('auth')
export class AuthController {
  private logger = new Logger(AuthController.name);

  constructor(private readonly authService: AuthService, private readonly apiConfigService: ApiConfigService) {}

  @Public()
  @Post('/login')
  @ApiOkResponse({
    description: 'Returns logged in user credentials and access & refresh tokens',
    type: [UserLoginResponseDto],
  })
  @ApiUnauthorizedResponse({
    description: 'Response when logging in with incorrect email or password',
  })
  async loginUser(
    @Res({ passthrough: true }) response: Response,
    @Body() userCredentials: UserLoginRequestDto,
  ): Promise<UserLoginResponseDto> {
    this.logger.debug(`Login request: ${JSON.stringify(userCredentials)}`);

    const userData = await this.authService.loginUser(userCredentials);
    this.logger.debug(`Logged in user: ${JSON.stringify(userData)}`);
    response.cookie('refreshToken', userData.userTokens.refreshToken, this.apiConfigService.tokenCookieOptions);
    this.logger.debug(`Set response refreshToken cookie`);
    return userData;
  }

  @Public()
  @ApiCookieAuth()
  @Get('/refresh')
  @ApiOkResponse({
    description: 'Returns user credentials and updated refresh & access tokens',
    type: [UserLoginResponseDto],
  })
  @ApiUnauthorizedResponse({
    description: 'Response when no or invalid refreshToken cookie is set',
  })
  async refreshUserTokens(
    @Res({ passthrough: true }) response: Response,
    @Req() request: Request,
  ): Promise<UserLoginResponseDto> {
    this.logger.debug(`Refresh request: ${JSON.stringify(request.body)}`);

    const cookies = <Cookies>request.cookies;
    this.logger.debug(`Cookies: ${JSON.stringify(cookies)}`);
    const userData = await this.authService.refreshUserTokens(cookies.refreshToken);
    response.cookie('refreshToken', userData.userTokens.refreshToken, this.apiConfigService.tokenCookieOptions);
    return userData;
  }

  @ApiBearerAuth()
  @Post('/logout')
  @ApiOkResponse({
    description: 'Ok if user successfully logged out',
  })
  @ApiUnauthorizedResponse({
    description: 'Response when no or invalid access token is present in authorization header',
  })
  async logoutUser(@Res({ passthrough: true }) response: Response, @Req() request: AuthorizedRequest): Promise<void> {
    this.logger.log(`Logging out ${JSON.stringify(request.user?.userId)}`);
    await this.authService.logoutUser(request.user?.userId as string);
    response.clearCookie('refreshToken');
    response.status(HttpStatus.OK);
  }
}
