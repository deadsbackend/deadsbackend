import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { PrismaModule } from '../prisma.module';
import { TokenModule } from './token.module';
import { PasswordService } from './password.service';
import { APP_GUARD } from '@nestjs/core';
import { JwtAuthGuard } from './jwtAuth.guard';
import { ApiConfigService } from '../config/apiConfig.service';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [PrismaModule, TokenModule, ConfigModule],
  controllers: [AuthController],
  providers: [
    AuthService,
    PasswordService,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
    ApiConfigService,
  ],
  exports: [AuthService, PasswordService],
})
export class AuthModule {}
