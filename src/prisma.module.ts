import { Module } from '@nestjs/common';
import { PrismaService } from './prisma.service';
import { ConfigModule } from '@nestjs/config';
import { ApiConfigService } from './config/apiConfig.service';

@Module({
  imports: [ConfigModule],
  controllers: [],
  providers: [PrismaService, ApiConfigService],
  exports: [PrismaService],
})
export class PrismaModule {}
