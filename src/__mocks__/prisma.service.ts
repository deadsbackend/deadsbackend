import { userCredentialStub } from '../user/tests/stubs/UserCredential.stub';
import { Prisma, UserCredential, UserDetail } from '@prisma/client';
import { UserCredentialsResponseDto } from '../user/dto/UserCredentials.response.dto';
import { userDetailStub } from '../user/tests/stubs/UserDetail.stub';

/* eslint-disable @typescript-eslint/no-unsafe-member-access */
export const PrismaService = jest.fn().mockReturnValue({
  userCredential: {
    create: jest.fn().mockImplementation((user): UserCredentialsResponseDto => {
      if (user.data.email === userCredentialStub()[0].email) {
        throw new Prisma.PrismaClientKnownRequestError('Duplicate email', 'P2002', 'MOCK');
      }
      return UserCredentialsResponseDto.serialize(userCredentialStub()[0]);
    }),
    update: jest.fn().mockImplementation((query) => {
      if (query.where.userId === userCredentialStub()[0].userId || query.where.id === userCredentialStub()[0].id) {
        if (query.include && query.include.detail) {
          return { ...userCredentialStub()[0], detail: userDetailStub() };
        }
        return userCredentialStub()[0];
      }
      return null;
    }),
    findUnique: jest.fn().mockImplementation((query): UserCredential | null => {
      let user: UserCredential | null = null;

      if (query.where?.userId) {
        user = query.where.userId === userCredentialStub()[0].userId ? userCredentialStub()[0] : null;
      }
      if (query.where?.email) {
        user = query.where.email === userCredentialStub()[0].email ? userCredentialStub()[0] : null;
      }
      return user;
    }),
    findMany: jest.fn().mockReturnValue(userCredentialStub()),
    findById: jest.fn().mockImplementation((id: number): UserCredential => {
      return userCredentialStub().filter((user) => user.id === id)[0];
    }),
  },
  userDetail: {
    findMany: jest.fn().mockImplementation((query): UserDetail[] | null => {
      let userDetail: UserDetail[] | null = null;

      if (query.where?.user?.is?.userId) {
        const records = userCredentialStub().filter((user) => user.userId === query.where?.user?.is?.userId);
        if (records.length) {
          const userRecordId = records[0].id;
          userDetail = userDetailStub().userCredentialId === userRecordId ? [userDetailStub()] : null;
        }
      }
      return userDetail;
    }),
    update: jest.fn().mockResolvedValue(userDetailStub()),
    create: jest.fn().mockResolvedValue(userDetailStub()),
  },
});
/* eslint-enable @typescript-eslint/no-unsafe-member-access */
