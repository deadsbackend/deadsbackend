import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { ConfigModule } from '@nestjs/config';
import { ConfigParameters } from './config/config.parameters';
import { AuthModule } from './auth/auth.module';
import { ItemModule } from './item/item.module';
import { InventoryModule } from './inventory/inventory.module';

@Module({
  imports: [UserModule, AuthModule, ItemModule, InventoryModule, ConfigModule.forRoot(ConfigParameters)],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
