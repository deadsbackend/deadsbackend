export enum EnvVariables {
  NODE_ENV = 'NODE_ENV',
  JWT_ACCESS_SECRET = 'JWT_ACCESS_SECRET',
  JWT_REFRESH_SECRET = 'JWT_REFRESH_SECRET',
  POSTGRES_DB = 'POSTGRES_DB',
  POSTGRES_USER = 'POSTGRES_USER',
  POSTGRES_PASSWORD = 'POSTGRES_PASSWORD',
  POSTGRES_HOST = 'POSTGRES_HOST',
  POSTGRES_PORT = 'POSTGRES_PORT',
  STATIC_FILES_DIRECTORY = 'STATIC_FILES_DIRECTORY',
}

export enum NodeEnv {
  development = 'development',
  production = 'production',
  test = 'test',
  staging = 'staging',
}
