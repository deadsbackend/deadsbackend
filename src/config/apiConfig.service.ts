import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { EnvVariables, NodeEnv } from './config.types';
import { CookieOptions } from 'express';

@Injectable()
export class ApiConfigService {
  constructor(private configService: ConfigService) {}

  get nodeEnvProd(): boolean {
    const currentEnv = this.configService.get<string>(EnvVariables.NODE_ENV);
    return currentEnv === NodeEnv.production;
  }

  get nodeEnvTest(): boolean {
    const currentEnv = this.configService.get<string>(EnvVariables.NODE_ENV);
    return currentEnv === NodeEnv.test;
  }

  get postgresUrl(): string {
    const postgresUser = this.configService.get<string>(EnvVariables.POSTGRES_USER);
    const postgresPassword = this.configService.get<string>(EnvVariables.POSTGRES_PASSWORD);
    const postgresHost = this.configService.get<string>(EnvVariables.POSTGRES_HOST);
    const postgresPort = this.configService.get<number>(EnvVariables.POSTGRES_PORT);
    const postgresDb = this.configService.get<string>(EnvVariables.POSTGRES_DB);

    // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
    return `postgresql://${postgresUser}:${postgresPassword}@${postgresHost}:${postgresPort}/${postgresDb}`;
  }

  get jwtAccessSecret(): string {
    return this.configService.get<string>(EnvVariables.JWT_ACCESS_SECRET) as string;
  }

  get jwtRefreshSecret(): string {
    return this.configService.get<string>(EnvVariables.JWT_REFRESH_SECRET) as string;
  }

  get staticFilesDirectory(): string {
    return this.configService.get<string>(EnvVariables.STATIC_FILES_DIRECTORY) as string;
  }

  get tokenCookieOptions(): CookieOptions {
    return {
      httpOnly: true,
      maxAge: 30 * 24 * 60 * 60 * 1000,
    };
  }
}
