import * as Joi from 'joi';
import { NodeEnv } from './config.types';
import { ConfigModuleOptions } from '@nestjs/config';

export const ConfigParameters: ConfigModuleOptions = {
  validationSchema: Joi.object({
    NODE_ENV: Joi.string()
      .valid(NodeEnv.development, NodeEnv.test, NodeEnv.staging, NodeEnv.production)
      .default(NodeEnv.development),
    JWT_ACCESS_SECRET: Joi.string().required(),
    JWT_REFRESH_SECRET: Joi.string().required(),
    POSTGRES_DB: Joi.string().default('deadsbackend'),
    POSTGRES_USER: Joi.string().default('deadsbackend'),
    POSTGRES_PASSWORD: Joi.string().default('deadsbackend'),
    POSTGRES_HOST: Joi.string().default('localhost'),
    POSTGRES_PORT: Joi.number().default(5432),
    STATIC_FILES_DIRECTORY: Joi.string().default('./static'),
  }),
  validationOptions: {
    allowUnknown: true,
    abortEarly: true,
  },
};
