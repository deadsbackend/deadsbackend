import { Item } from '@prisma/client';

export const itemsStub = (): Item[] => {
  return [
    {
      id: 1,
      itemId: 'f4024e9e-d67f-42ba-9d5b-00cb2f4bf34f',
      title: 'First item',
      price: 10,
      partNumber: 'IT-001',
      description: 'First item description',
      createdAt: new Date('2022-01-03'),
      updatedAt: new Date(Date.now()),
    },
    {
      id: 2,
      itemId: '67be4e35-d93c-4d9e-8ba5-6918213b9b70',
      title: 'Second item',
      price: 1000,
      partNumber: 'PN-22-11',
      description: 'Second item description',
      createdAt: new Date('2022-01-02'),
      updatedAt: new Date(Date.now()),
    },
  ];
};
