export enum ItemErrorMessages {
  DUPLICATE_ITEMID = 'duplicate itemId',
  UNKNOWN_SERVER_ERROR = 'unknown server error while managing items',
  ITEMID_NOT_FOUND = 'itemId not found',
  ERROR_UPDATING_ITEM = 'error updating item',
  ITEM_TITLE_NOT_FOUND = 'no items with provided title found',
}
