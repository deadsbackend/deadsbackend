export enum ItemImageErrorMessages {
  INVALID_IMAGE_FORMAT = 'Invalid image format, only JPEG, PNG or GIF images are accepted',
  ERROR_MOVING_TO_STATIC_DIRECTORY = 'Error moving item image to static files directory',
  UNKNOWN_SERVER_ERROR = 'Unknown server error while getting item images',
  ITEM_IMAGE_NOT_FOUND = 'Specified item image ID not found',
}
