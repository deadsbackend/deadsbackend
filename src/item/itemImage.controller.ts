import { Controller, Delete, Get, Param, Post, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { ItemImageService } from './itemImage.service';
import { FilesInterceptor } from '@nestjs/platform-express';
import { UuidParam } from '../types/uuid/uuid.param';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { diskStorage } from 'multer';
import { imageFileFilter, newItemImageName } from './itemImage.utils';
import { ItemImagesResponseDto } from './dto/itemImages.response.dto';
import { Public } from '../auth/jwtAuth.guard';

@ApiTags('Item Images')
@Controller('itemImages')
export class ItemImageController {
  constructor(private itemImageService: ItemImageService) {}

  @Post('/upload/:id')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        files: {
          type: 'array',
          items: {
            type: 'string',
            format: 'binary',
          },
        },
      },
    },
  })
  @ApiOkResponse({
    description: 'Files successfully uploaded',
  })
  @ApiBadRequestResponse({
    description: 'Unsupported image format, only JPEG/PNG/GIF images are accepted',
  })
  @ApiBearerAuth()
  @UseInterceptors(
    FilesInterceptor('images', 20, {
      storage: diskStorage({
        destination: './static/tmp',
        filename: newItemImageName,
      }),
      fileFilter: imageFileFilter,
    }),
  )
  uploadItemImages(
    @Param() itemId: UuidParam,
    @UploadedFiles() images: Array<Express.Multer.File>,
  ): Promise<ItemImagesResponseDto> {
    console.log(JSON.stringify(images));
    return this.itemImageService.uploadItemImages(itemId.id, images);
  }

  @Public()
  @Get(':id')
  @ApiOkResponse({
    description: 'Returns item ID and item images file names',
    type: ItemImagesResponseDto,
  })
  @ApiNotFoundResponse({
    description: 'Returns 404 code if no item images found',
  })
  async getItemImages(@Param() itemId: UuidParam): Promise<ItemImagesResponseDto> {
    return this.itemImageService.getItemImages(itemId.id);
  }

  @Delete(':id')
  @ApiOkResponse({
    description: 'Returns if item image successfully deleted',
  })
  @ApiNotFoundResponse({
    description: 'Returns 404 if item image not found',
  })
  @ApiBearerAuth()
  async deleteItemImageById(@Param() imageId: UuidParam): Promise<void> {
    await this.itemImageService.deleteItemImageById(imageId.id);
  }
}
