import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { ApiConfigService } from '../config/apiConfig.service';
import * as path from 'path';
import { ItemErrorMessages } from './types/itemErrorMessages';
import * as fs from 'fs';
import { ItemImageErrorMessages } from './types/itemImageErrorMessages';
import { ItemImagesResponseDto } from './dto/itemImages.response.dto';

@Injectable()
export class ItemImageService {
  private readonly logger = new Logger(ItemImageService.name);

  constructor(private prismaService: PrismaService, private apiConfigService: ApiConfigService) {}

  async uploadItemImages(itemId: string, images: Array<Express.Multer.File>): Promise<ItemImagesResponseDto> {
    this.logger.debug(`Uploading images for item ${itemId}`);

    const item = await this.prismaService.item.findUnique({
      where: {
        itemId,
      },
    });
    if (!item) {
      this.logger.debug(`Item id: ${itemId} not found while trying to upload item images.`);
      throw new HttpException({ message: ItemErrorMessages.ITEMID_NOT_FOUND }, HttpStatus.NOT_FOUND);
    }

    const createdItemImages: Array<string> = [];
    const itemImagesDirectory = path.join(this.apiConfigService.staticFilesDirectory, itemId);
    this.logger.debug(`Moving uploaded item images to ${itemImagesDirectory}...`);
    await Promise.all(
      images.map(async (imageFile) => {
        if (!fs.existsSync(itemImagesDirectory)) {
          this.logger.debug(`Directory ${itemImagesDirectory} does not exist, creating...`);
          fs.mkdirSync(itemImagesDirectory, { recursive: true });
        }
        try {
          fs.renameSync(
            path.join('./static/tmp', imageFile.filename),
            path.join(itemImagesDirectory, imageFile.filename),
          );
          const itemImageId = imageFile.filename.split('.')[0];
          await this.prismaService.itemImage.create({
            data: {
              itemImageId,
              fileName: imageFile.filename,
              item: {
                connect: {
                  itemId,
                },
              },
            },
          });
          createdItemImages.push(imageFile.filename);
        } catch (e) {
          this.logger.error(
            `Error moving item image ${imageFile.filename} to ${itemImagesDirectory}/${
              imageFile.filename
            }: ${JSON.stringify(e)}`,
          );
          throw new HttpException(
            { message: ItemImageErrorMessages.ERROR_MOVING_TO_STATIC_DIRECTORY },
            HttpStatus.INTERNAL_SERVER_ERROR,
          );
        }
        this.logger.debug(`Successfully moved item image ${imageFile.filename}.`);
      }),
    );

    return {
      itemId,
      images: createdItemImages,
    };
  }

  async getItemImages(itemId: string): Promise<ItemImagesResponseDto> {
    this.logger.debug(`Getting item images for item ${itemId}...`);
    const item = await this.prismaService.item.findUnique({
      where: {
        itemId,
      },
    });

    if (item) {
      const itemImages = await this.prismaService.itemImage.findMany({
        where: {
          itemId: item.id,
        },
      });

      return {
        itemId,
        images: itemImages.map((itemImage) => itemImage.itemImageId),
      };
    } else {
      throw new HttpException({ message: ItemErrorMessages.ITEMID_NOT_FOUND }, HttpStatus.NOT_FOUND);
    }
  }

  async deleteItemImageById(imageId: string): Promise<void> {
    this.logger.debug(`Deleting item image ${imageId}...`);

    const itemImage = await this.prismaService.itemImage.findUnique({
      where: {
        itemImageId: imageId,
      },
      include: {
        item: true,
      },
    });

    if (itemImage) {
      const itemImageFilename = path.join(
        this.apiConfigService.staticFilesDirectory,
        itemImage.item.itemId,
        `${itemImage.itemImageId}.jpg`,
      );

      fs.unlinkSync(itemImageFilename);
      this.logger.debug(`Removed item image ${itemImageFilename}`);
      await this.prismaService.itemImage.delete({
        where: {
          id: itemImage.id,
        },
      });
      this.logger.debug(`Deleted ${imageId} from database.`);
    } else {
      throw new HttpException({ message: ItemImageErrorMessages.ITEM_IMAGE_NOT_FOUND }, HttpStatus.NOT_FOUND);
    }
  }
}
