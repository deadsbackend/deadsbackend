import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsOptional, IsPositive, IsString } from 'class-validator';

export class ItemCreateDto {
  @ApiProperty({ description: 'Item title' })
  @IsNotEmpty()
  @IsString()
  title: string;

  @ApiProperty({ description: 'Optional item part number' })
  @IsOptional()
  @IsString()
  partNumber?: string;

  @ApiProperty({ description: 'Optional item description' })
  @IsOptional()
  @IsString()
  description?: string;

  @ApiProperty({ description: 'Item price' })
  @IsNumber({ maxDecimalPlaces: 2 })
  @IsPositive()
  price: number;
}
