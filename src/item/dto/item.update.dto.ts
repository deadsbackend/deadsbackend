import { ItemCreateDto } from './item.create.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';

export class ItemUpdateDto {
  @ApiProperty({ description: 'Item ID for update' })
  @IsNotEmpty()
  @IsUUID()
  itemId: string;

  @IsNotEmpty()
  itemDetail: Partial<ItemCreateDto>;
}
