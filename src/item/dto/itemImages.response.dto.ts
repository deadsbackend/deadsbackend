import { ApiProperty } from '@nestjs/swagger';

export class ItemImagesResponseDto {
  @ApiProperty({
    description: 'Item ID (UUIDv4)',
  })
  itemId: string;

  @ApiProperty({
    description: 'Array of item images file names',
  })
  images: Array<string>;
}
