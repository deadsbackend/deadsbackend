import { ApiProperty } from '@nestjs/swagger';
import { Item } from '@prisma/client';

export class ItemResponseDto {
  @ApiProperty({
    description: 'Unique item id (UUIDv4)',
  })
  itemId: string;

  @ApiProperty({
    description: 'Optional item part number',
  })
  partNumber?: string;

  @ApiProperty({
    description: 'Item title',
  })
  title: string;

  @ApiProperty({
    description: 'Optional item description',
  })
  description?: string;

  @ApiProperty({
    description: 'Item price',
  })
  price: number;

  @ApiProperty({
    description: 'Item creation date and time',
  })
  createdAt: Date;

  @ApiProperty({
    description: 'Item last update date and time',
  })
  updatedAt: Date;

  static serialize(item: Item): ItemResponseDto {
    return {
      itemId: item.itemId,
      partNumber: item.partNumber as string,
      title: item.title,
      description: item.description as string,
      price: item.price,
      createdAt: item.createdAt,
      updatedAt: item.updatedAt,
    };
  }
}
