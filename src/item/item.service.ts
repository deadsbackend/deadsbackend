import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import * as fs from 'fs';
import { PrismaService } from '../prisma.service';
import { ItemCreateDto } from './dto/item.create.dto';
import { ItemResponseDto } from './dto/item.response.dto';
import { Prisma } from '@prisma/client';
import { ItemErrorMessages } from './types/itemErrorMessages';
import { ItemUpdateDto } from './dto/item.update.dto';
import { ItemImageService } from './itemImage.service';
import { ApiConfigService } from '../config/apiConfig.service';
import * as path from 'path';

@Injectable()
export class ItemService {
  private readonly logger = new Logger(ItemService.name);

  constructor(
    private prismaService: PrismaService,
    private itemImageService: ItemImageService,
    private apiConfigService: ApiConfigService,
  ) {}

  async createItem(newItemDto: ItemCreateDto): Promise<ItemResponseDto> {
    const itemId = uuidv4();
    const now = new Date(Date.now());
    this.logger.debug(`Creating "${newItemDto.title}" with itemId: ${itemId}`);
    try {
      const newItem = await this.prismaService.item.create({
        data: {
          itemId,
          ...newItemDto,
          createdAt: now,
          updatedAt: now,
        },
      });
      return ItemResponseDto.serialize(newItem);
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        this.logger.debug(`Caught exception with Prisma error code ${e.code}`);
        if (e.code === 'P2002') {
          this.logger.error(`Error creating duplicate item: ${JSON.stringify(itemId)}`);
          throw new HttpException({ message: ItemErrorMessages.DUPLICATE_ITEMID }, HttpStatus.CONFLICT);
        }
      }
    }
    this.logger.error(`Unknown error while creating "${newItemDto.title}" with itemId: ${itemId}`);
    throw new HttpException({ message: ItemErrorMessages.UNKNOWN_SERVER_ERROR }, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  async getAllItems(): Promise<ItemResponseDto[]> {
    this.logger.debug(`Getting all items...`);
    const responseData = await this.prismaService.item.findMany();
    const items: ItemResponseDto[] = [];

    responseData.map((item) => {
      items.push(ItemResponseDto.serialize(item));
    });
    return items;
  }

  async getItemById(itemId: string): Promise<ItemResponseDto> {
    this.logger.debug(`Getting item with itemId: ${itemId}`);
    const item = await this.prismaService.item.findUnique({
      where: {
        itemId,
      },
    });

    if (item) {
      return ItemResponseDto.serialize(item);
    }

    throw new HttpException({ message: ItemErrorMessages.ITEMID_NOT_FOUND }, HttpStatus.NOT_FOUND);
  }

  async getItemsByTitle(title: string): Promise<ItemResponseDto[]> {
    this.logger.debug(`Getting items with title containing "${title}"...`);
    const items: ItemResponseDto[] = [];
    const responseData = await this.prismaService.item.findMany({
      where: {
        title: {
          contains: title,
          mode: 'insensitive',
        },
      },
    });

    if (items) {
      responseData.map((item) => {
        items.push(ItemResponseDto.serialize(item));
      });
      return items;
    }

    this.logger.error(`Unknown server error while trying to get items with title: "${title}"`);
    throw new HttpException({ message: ItemErrorMessages.UNKNOWN_SERVER_ERROR }, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  async updateItemById(itemUpdateDto: ItemUpdateDto): Promise<ItemResponseDto> {
    try {
      this.logger.debug(`Updating item: ${itemUpdateDto.itemId} with ${JSON.stringify(itemUpdateDto.itemDetail)}`);
      const now = new Date(Date.now());
      const updatedItem = await this.prismaService.item.update({
        where: {
          itemId: itemUpdateDto.itemId,
        },
        data: {
          ...itemUpdateDto.itemDetail,
          updatedAt: now,
        },
      });

      if (updatedItem) {
        return ItemResponseDto.serialize(updatedItem);
      }
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        this.logger.debug(`Caught exception with Prisma error code ${e.code}`);
        if (e.code) {
          switch (e.code) {
            case 'P2001': {
              this.logger.error(`Error updating item ${JSON.stringify(itemUpdateDto.itemId)}: itemId not found`);
              throw new HttpException({ message: ItemErrorMessages.ITEMID_NOT_FOUND }, HttpStatus.NOT_FOUND);
            }
            default: {
              this.logger.error(
                `Unknown error while updating itemId: ${itemUpdateDto.itemId} with ${JSON.stringify(
                  itemUpdateDto.itemDetail,
                )}`,
              );
              throw new HttpException(
                { message: ItemErrorMessages.UNKNOWN_SERVER_ERROR },
                HttpStatus.INTERNAL_SERVER_ERROR,
              );
            }
          }
        }
      }
    }
    this.logger.error(
      `Unknown error while updating itemId: ${itemUpdateDto.itemId} with ${JSON.stringify(itemUpdateDto.itemDetail)}`,
    );
    throw new HttpException({ message: ItemErrorMessages.UNKNOWN_SERVER_ERROR }, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  async deleteItemById(itemId: string): Promise<boolean> {
    try {
      this.logger.debug(`Deleting item with itemId: ${itemId}...`);
      const itemToDelete = await this.prismaService.item.findUnique({
        where: {
          itemId,
        },
        include: {
          itemImages: true,
        },
      });

      if (itemToDelete) {
        await Promise.all(
          itemToDelete.itemImages.map(async (itemImage) => {
            await this.itemImageService.deleteItemImageById(itemImage.itemImageId);
          }),
        );
      }

      const deletedItem = await this.prismaService.item.delete({
        where: {
          itemId,
        },
      });

      fs.rmdirSync(path.join(this.apiConfigService.staticFilesDirectory, itemId));

      if (deletedItem) {
        return true;
      }
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        this.logger.debug(`Caught exception with Prisma error code ${e.code}`);
        if (e.code === 'P2002' || e.code === 'P2025 ') {
          this.logger.error(`Error deleting item: itemId ${JSON.stringify(itemId)} not found`);
          throw new HttpException({ message: ItemErrorMessages.ITEMID_NOT_FOUND }, HttpStatus.NOT_FOUND);
        }
      }
    }
    this.logger.error(`Unknown error while deleting item with itemID: ${itemId}`);
    throw new HttpException({ message: ItemErrorMessages.UNKNOWN_SERVER_ERROR }, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
