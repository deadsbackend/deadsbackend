import { Request } from 'express';
import { HttpException, HttpStatus } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import { ItemImageErrorMessages } from './types/itemImageErrorMessages';

export const imageFileFilter = (
  req: Request,
  file: Express.Multer.File,
  callback: { (error: HttpException | null, isValid: boolean): void },
) => {
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
    return callback(
      new HttpException({ message: ItemImageErrorMessages.INVALID_IMAGE_FORMAT }, HttpStatus.BAD_REQUEST),
      false,
    );
  }
  callback(null, true);
};

export const newItemImageName = (
  req: Request,
  file: Express.Multer.File,
  callback: { (error: HttpException | null, fileName: string): void },
) => {
  const fileExtension = file.originalname.split('.').slice(-1)[0];
  const newUUID = uuidv4();

  callback(null, `${newUUID}.${fileExtension}`);
};
