import { Module } from '@nestjs/common';
import { PrismaModule } from '../prisma.module';
import { ItemService } from './item.service';
import { ItemController } from './item.controller';
import { ItemImageService } from './itemImage.service';
import { ItemImageController } from './itemImage.controller';
import { MulterModule } from '@nestjs/platform-express';
import { ApiConfigService } from '../config/apiConfig.service';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    PrismaModule,
    MulterModule.register({
      dest: './static/tmp',
    }),
    ConfigModule,
  ],
  providers: [ItemService, ItemImageService, ApiConfigService],
  controllers: [ItemController, ItemImageController],
})
export class ItemModule {}
