import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { ItemService } from './item.service';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ItemCreateDto } from './dto/item.create.dto';
import { ItemResponseDto } from './dto/item.response.dto';
import { Public } from '../auth/jwtAuth.guard';
import { UuidParam } from '../types/uuid/uuid.param';

@ApiTags('Items')
@Controller('items')
export class ItemController {
  constructor(private itemService: ItemService) {}

  @Post('/create')
  @ApiCreatedResponse({
    description: 'Newly created Item object',
    type: ItemResponseDto,
  })
  @ApiBadRequestResponse({
    description: 'Response when invalid item parameters are sent in request',
  })
  @ApiConflictResponse({
    description: 'Response when trying to create item which already exists',
  })
  @ApiBearerAuth()
  async createItem(@Body() newItemDto: ItemCreateDto): Promise<ItemResponseDto> {
    return this.itemService.createItem(newItemDto);
  }

  @Public()
  @Get()
  @ApiOkResponse({
    description: 'Returns all items',
    type: [ItemResponseDto],
  })
  async getAllItems(): Promise<ItemResponseDto[]> {
    return this.itemService.getAllItems();
  }

  @Public()
  @Get('/bytitle')
  @ApiOkResponse({
    description: 'Returns all items which title contains "title" parameter',
    type: [ItemResponseDto],
  })
  async getItemsByTitle(@Query('title') title: string): Promise<ItemResponseDto[]> {
    return this.itemService.getItemsByTitle(title);
  }

  @Public()
  @Get('/:itemId')
  @ApiOkResponse({
    description: 'Returns item with specific ID',
    type: ItemResponseDto,
  })
  @ApiNotFoundResponse({
    description: 'Item with specified itemId not found',
  })
  async getItemById(@Param() itemId: UuidParam): Promise<ItemResponseDto> {
    return this.itemService.getItemById(itemId.id);
  }

  @Patch('/:itemId')
  @ApiOkResponse({
    description: 'Returns updated item object',
    type: ItemResponseDto,
  })
  @ApiNotFoundResponse({
    description: 'Item with provided itemId not found in database',
  })
  @ApiInternalServerErrorResponse({
    description: 'Unknown server error while updating item',
  })
  @ApiBearerAuth()
  async updateItemById(
    @Param() itemId: UuidParam,
    @Body() itemDetail: Partial<ItemCreateDto>,
  ): Promise<ItemResponseDto> {
    return this.itemService.updateItemById({ itemId: itemId.id, itemDetail });
  }

  @Delete('/:id')
  @ApiOkResponse({
    description: 'Returns true if item is successfully deleted',
  })
  @ApiNotFoundResponse({
    description: 'Item with provided itemId not found',
  })
  @ApiInternalServerErrorResponse({
    description: 'Unknown server error while deleting item',
  })
  @ApiBearerAuth()
  async deleteItemById(@Param() itemId: UuidParam): Promise<boolean> {
    return this.itemService.deleteItemById(itemId.id);
  }
}
