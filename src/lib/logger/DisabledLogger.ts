import { LoggerService } from '@nestjs/common';

/* eslint-disable @typescript-eslint/no-unused-vars, @typescript-eslint/no-explicit-any */
export class DisabledLogger implements LoggerService {
  log(message: string): any {}

  error(message: string, trace: string): any {}

  warn(message: string): any {}

  debug(message: string): any {}

  verbose(message: string): any {}
}

/* eslint-enable @typescript-eslint/no-unused-vars, @typescript-eslint/no-explicit-any */
