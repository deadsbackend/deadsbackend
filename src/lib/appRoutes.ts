export const AppRoutes = {
  auth: {
    root: '/auth',
    login: '/auth/login',
    refreshTokens: '/auth/refresh',
    logout: '/auth/logout',
  },
  items: {
    root: '/items',
  },
  users: {
    root: '/user',
  },
  itemImages: {
    root: '/itemImages',
    upload: '/itemImages/upload',
  },
};
