/*
  Warnings:

  - You are about to drop the column `accessToken` on the `UserCredential` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "UserCredential" DROP COLUMN "accessToken";
