/*
  Warnings:

  - A unique constraint covering the columns `[fileName]` on the table `ItemImage` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `fileName` to the `ItemImage` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "ItemImage" ADD COLUMN     "fileName" TEXT NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "ItemImage_fileName_key" ON "ItemImage"("fileName");
