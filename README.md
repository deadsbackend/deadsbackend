# Dead's Backend Learning Project

## What's it?

I'm learning to build backend applications and this project shows my current progress. It's in ongoing development so it
will change drastically over time.

I've started it as a simple Internet shop backend with some basic features:

- users & user details
- items and item inventory
- user's orders

## Technology

As for now, I'm building the project with [Nest JS](https://nestjs.com) with [PostgreSQL](https://postgresql.org) as
database and [Prisma](https://prisma.io) as ORM. Since I'm not bound to any specific technology I'm trying to use
state-of-the-art approaches while not running for hot but not so reliable things.

I'm planning to over-engineer some aspects just to train myself and get used with some architectural patterns and
methodologies such as Domain Driven Design, Hexagonal architecture and so on.

At this time the project is bundled to run with Docker Compose on local machine. In the future I plan to configure
Gitlab CI/CD to roll out the backend to Kubernetes & Postgres cluster.

## How to use

1. Clone the repository
2. Copy `.env.example` file to `.env` and edit variables to suite your needs or leave the defaults — the project should
   run on local machine without any problems.
3. Run `docker compose up -d`
    1. Postgres database is stored in `pgdata` Docker volume
    2. Static files are stored in `static` Docker volume
4. Run `npm run start:migrate:dev` to start development server and apply all database migrations
5. Navigate to [Swagger API documentation](http://localhost) and discover API endpoints and requests parameters.
6. All static files (item images) are available at `http://localhost/static/<itemId>/<filename.jpg>` and are served by
   nginx
7. Tests:
    1. Unit tests can be run on your local machine with `npm run test`
    2. Integration tests can be run on your local machine with `./utils/e2e-docker-tests.sh`. This will bring up
       Postgres test database and testing Docker containers and run tests.
    3. Unit tests are automatically run on all branches
    4. Integration tests are automatically run only on merge requests and main branch

## Contributing

I have not thought when I'll be open for contributions but feel free to contact me with any suggestions until then.
