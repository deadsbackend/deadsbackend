FROM node:16.13.2-alpine as development
WORKDIR /deadsbackend

COPY package.json package-lock.json ./
RUN npm ci --cache .npm --prefer-offline

COPY . .
RUN npx prisma generate
RUN npm run prebuild && npm run build

FROM node:16.13.2-alpine as production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /deadsbackend

COPY package.json package-lock.json ./
RUN npm ci --cache .npm --prefer-offline

COPY . .
COPY --from=development /deadsbackend/dist ./dist

EXPOSE 5000
CMD ["node", "dist/main"]



