#!/bin/bash

docker-compose -f ./docker-compose.e2e-tests.yml up --remove-orphans --abort-on-container-exit --exit-code-from deadsbackend --build --force-recreate deadsbackend
docker-compose -f ./docker-compose.e2e-tests.yml stop
docker-compose -f ./docker-compose.e2e-tests.yml rm -v -f
docker image prune -f
